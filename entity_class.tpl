  <?php

  /**
  * {{ENTITY CLASS}} entity class.
  */
  class {{ENTITY CLASS}} extends Entity {
    protected function defaultLabel() {
      return $this->${{LABEL FIELD}};
    }
    protected function defaultUri() {
      return array('path' => '{{ENTITY DOMAIN}}/' . $this->identifier());
    }
  }