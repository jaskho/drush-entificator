<?php

# 
# entity_domain                 # eg university
#                               #  + UI views, forms, etc will show up 
#                               #    under /university
#                               #  + Table will be prefixed with university_
# entity_id=entity_name         # eg student_group
#                               #  + Table will be named university_student_group
#                               #  + List view will be at /univerity/student_group/list
#                               #  + Add form will be at /univerity/student_group/add
#                               #  + Item view will be at /univerity/student_group/%
#                               #  + Item edit form will be at /univerity/student_group/%/edit
# entity_plural_name            # eg Student Groups
# fields_schema_source          # For future use: specify an existing table, 
#                               #  json array, etc to be used as source for 
#                               #  fields data
# fields=field_1,field_2,...    # eg id,name
#                               #  + See noted under pkfld
# pkfld=field_1                 # eg id
#                               #  + optional
#                               #  + Entity MUST have an auto increment pk field
#                               #  + If $pkfld is NOT specified, a field will be
#                               #    ADDED to $fields, named 'id'
#                               #  + If $pkfld IS specified, a matching field must
#                               #    be included in $fields.
# labelfld=field_x              # eg name
#



# ------------------------------------------------
#    Load templates
# ------------------------------------------------
$ENTITY_CLASS_TPL=
$ENTITY_UI_CLASS_TPL=
$ADMIN_INC_TPL=
$HOOK_SCHEMA_TPL=
$HOOK_MENU_TPL=
$HOOK_PERM_TPL=
$HOOK_ENT_INFO_TPL=
$API_FUNCTIONS_INC_TPL=
$API_INC_TPL=
$HOOK_VIEWS_API_TPL=
$HOOK_VIEWS_DATA_TPL=
$HOOK_VIEWS_DEFAULT_VIEWS_TPL=


class Entificator
{
  $schema_importer_plugins;

  function bootstrap(){
    $plugins = module_invoke_all('entificator_plugin');
    $this->schema_importer_plugins = $plugins['schema_importer'];
  }

}


# ------------------------------------------------
#    Create entity ui controller class/file
# ------------------------------------------------

  touch lib/{{ENTITY CLASS}}controller.php
  create file from template