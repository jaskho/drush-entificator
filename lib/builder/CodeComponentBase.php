<?php
namespace Drupal\builder;

/**
 * Code Component base class.
 * Utility class for generating application code from templates.
 * 
 * A "code component" is a logical aspect of an application for which
 * one or more output files will be generated.
 * 
 * A Code Component may map to multiple templates and output files:
 *     code component A:
 *        + code template x  --  application file x
 *        + code template y  --  application file y
 *        + code template z  --  application file z
 * For many applications, only one Code Component is required.
 * 
 * However, in cases where a template might be used multiple times
 * within an application (ie, there will be multiple versions of a particular
 * kind of file), a separate, child Code Component should be usde.
 * 
 * For example, consider an application that contains multiple classes,
 * XFoo, YFoo & ZFoo, derived from a single base class, FooBase.
 * 
 * The body of the application, along with the template/processing for
 * the FooBase base class, would be modeled with one Code Component, while
 * the derived classes would be modeled with a second Code Component, as
 * children of the app-level Code Component:
 * 
 *    + APP CODE COMPONENT 
 *    
 *       + Templates/output
 *           + FooBase template            >> lib/FooBase.php
 *           + AppMain template            >> index.php
 *           ...
 *           
 *       + Child Code Components
 *       
 *           + Foo
 *              + AbcFoo
 *                 + FooDrived template    >> lib/AbcFoo.php
 *              + XyzFoo
 *                 + FooDrived template    >> lib/XyzFoo.php
 *
 * USAGE
 *
 * 1. Create template code files, place in a directory accessible by 
 *    your builder script.
 *    If necessary, create an output directory.
 * 2. Implement CodeComponentBase derived class, eg, MyAppComponent.
 * 3. Implement child component classes, if necessary.
 * 4. Create a "builder script" to execute the code generation. 
 *    Ensure your script has read access to the templates directory
 *    and write access to the output directory.
 *    Example:
 *    <code>
 *      // Configure app build schema
 *      $schema = array(...);
 *  
 *      // Instantiate module code component object. This code assumes
 *      // you've given the name "MyAppComponent" to your class derived
 *      // from CodeComponentBase.
 *      $cfg = array(
 *        'base_template_path' => 'path/to/templates/folder',
 *        'schema' => $schema,
 *      );
 *      $builder = new MyAppComponent($cfg);
 *  
 *      // Perform the build
 *      $builder->build();
 *    </code>    
 * 
 */

abstract class CodeComponentBase {
  
  /**
   * Processing flags
   * @var array
   */
  protected $state = array(
    'dbgop' => true,
    'template_map_generated' => false,
    'token_replacements_generated' => false,
  );

  /**
   * State flag: indicate component passed validation checks
   */
  const VALIDATION_PASSED = 'ok';

  public $type = null;

  protected $component_key = null;

  protected $parent = null;

  protected $base_template_path = null;

  protected $output_path = null;

  /**
   * Child types and implementing classes.
   * @var structured array of component type to class name mappings.
   *      {type_key}:{class_name} pairs.
   *      array(
   *         'app' => {class name},
   *         'children' => array(
   *            {type} => {class name},
   *            ...
   *         )
   *       )
   * This is treated as a global property. Implementing classes should
   * access this property via getter/setter functions, with child components
   * returning a reference to the App (root/top-level/whatever) component's
   * property.
   * NOTE: implementations using child components must populate this array.
   */
  protected $component_types = array(
    'app' => null,
    'children' => array(),
  );

  /**
   * Implementation-specific array of data for building
   * out code.
   * @var Array
   */
  protected $schema = null;

  /**
   * Child components
   * @var array
   *      Implementation-specific, but typically arrays keyed by type:
   *        array(
   *          'child component type x' => array
   *             {child component x.1},
   *             {child component x.2},
   *             ...
   *           ),
   *           ...
   *        )
   */
  protected $children = array();
  
  /**
   * Component template mappings
   * @var array of {template_key}:{file path}
   *      {file path}
   *        May contain replacement tokens for generating dynamic
   *        paths and filenames.
   *      NOTE: a useful pattern is to use the file path+basename as the key.
   *      EG:
   *      array(
   *        'lib/FooController' => 'lib/%FOO_TYPE%FooController.php'),
   *        ...
   *      )
   */
  protected $template_map = null;
  
  /**
   * Component template mappings with tokens replaced
   * @var array of {template_key}:{file path}
   */
  protected $output_map = null;

  /**
   * Raw template file contents
   * @var array of {template_key}:{template file contents}
   *      {template_key} maps to keys in $template_map
   */
  protected $templates_raw = array();

  /**
   * In-process template file contents for generating output files
   * @var array of {template_key}:{output file contents}
   *      {template_key} maps to keys in $template_map
   */
  protected $templates_output;

  /**
   * Array of token keys.
   * @var array of string token keys
   * For a template token {{TOKEN_STRING}}, use 
   *   $token_keys = array(
   *     'TOKEN_KEY',
   *   );
   */
  protected $token_keys = array();

  /**
   * Token keys and replacement values
   * @var array of {token}:{replacement value} pairs.
             * Array structure:
             *   $tokens = array(
             *     'app' = {tokens array},
             *     'children' => array(
             *       'type' => array(
             *          {co})))
             * This is treated as an "application global" property; child component
             * classes should be instantiated with $this->tokens set to a reference
             * to the owning object's $tokens property.
             * Note that token tags inhabit a global namespace relative to the
             * application being generated. Although templates are owned by particular
             * CodeComponent derived classes, tokens should not be thought of as 
             * "local" to a template or CodeComponent.
   */
  protected $tokens = array();

  /**
   * Class constuctor
   * @param array $config
   *        {Code Component Config} array
   *        Base class keys:
   *         + base_template_path  -- path to template files
   *         + output_path  -- path to root output directory
   *         + schema  -- implementation-defined configuration array
   *         + owner  -- ref to parent Component module, in the case of a child
   *           component (in which case this reference is required)
   *         + templates (NOT USED)
   *         base_template_path, output_path, and schema are required. owner is
   *         required for child components.
   */
  public function __construct($config = null) {

    $this->base_template_path = $config['base_template_path'];
    $this->output_path = $config['output_path'];
    $this->schema = $config['schema'];
    $this->owner = @$config['owner'];

    $this->preprocess();
  } 

  /**
   * Validate component configuration.
   * @param  array $config {Code Component Config} array
   * @return misc         
   *          + If no error: CONST entificator\CodeComponentBase::VALIDATION_PASSED
   *          + If error: (string) description of error
   */
  protected function validate($config) {

    $err = CodeComponentBase::VALIDATION_PASSED;

    if(!$config) $err = 'Configuration not provided';

    if(!$err && ! @$config['base_template_path']) 
          $err = 'Base template path is required';

    if(!$err && ! @$config['schema']) 
          $err = 'Component schema is required';

    return $err;
  }

  /**
   * Process schema/configuration (apply defaults and dynamic values), if necessary
   * @return none
   */
  protected function preprocess() {}

  /**
   * Get replacement text for token.
   * @param  string $token Token key from $this->tokens
   * @return string        Replacement text for token.
   * This function triggers generateTokenReplacement() if the token value has not been
   * generated.
   */
  protected function getTokenValue($token) {
    if(!array_key_exists($token, $this->tokens)) {
      $this->generateTokenReplacement($token);
    } 
    return $this->tokens[$token];
  }

  /**
   * Apply further schema/configuration processing, if necessary. (This is
   * useful when this component's configuration depends on child component
   * values.)
   * @return [type] [description]
   * @return none
   * NOTE: this function must be applied recursively. Overriding classes must
   *       take care to call parent::postprocess() or manually recurse through
   *       child components.
   */
  protected function postprocess() {
    foreach($this->getChildComponents() as $child) {
      $child->postprocess();
    }
  }

  /**
   * Configure templatesand output mapx, applying dynamic path/filenames if necessary.
   * @param  misc $templates 
   *         NOT USED (see notes below)
   * @return none
   * At this point (see note below) all implementing classes must override
   * this function.
   * Implementing classes using dynamic path/filenames should override this
   * method in order to apply the required logic.
   * @todo : As things stand, there's little benefit to using a function
   *         versus a class property for this, but we may in the future want to
   *         support dynamically configured templates.
   */
  protected function mapTemplates($template_map = array(), $replacements = null) {
    if($this->state['dbgop']) print "mapTemplates() - {$this->type} / {$this->component_key}\n";

    if($this->state['template_map_generated']) {
      return;
    } else {
      $this->state['template_map_generated'] = true;
    }

    // Configure
    $this->template_map = $template_map;

    // Copy for processing/output
    $this->output_map = $this->template_map;

    // Replace filepath/name tokens
    if($replacements) {
      $this->template_map = str_replace(array_keys($replacements['template_map']), $replacements['template_map'], $this->template_map);
      $this->output_map = str_replace(array_keys($replacements['output_map']), $replacements['output_map'], $this->output_map);
    }

    // Recurse child components
    foreach($this->getChildComponents() as $child) $child->mapTemplates();
  }

  /**
   * Build output files
   * @return none
   */
  public function build() {
    if($this->state['dbgop']) print "build() - {$this->type} / {$this->component_key}\n";

    // Post-process (recursive)
    $this->postprocess();

    // Create templates map ( ??? recursive)
    // @Todo: see notes at CodeComponentBase::mapTemplates()
    $tpls = null;
    $this->mapTemplates($tpls);

    // Load templates
    $this->loadTemplates();

    // Process templates
    $this->generateTokenReplacements();
    $this->expandTemplateTokens();
    $this->processTemplates();
    
    // Recurse child components
    foreach($this->getChildComponents() as $child) $child->build();
  }

  /**
   * Write output files
   * @return none
   */
  public function write() {
    if($this->state['dbgop']) print "write() - {$this->type} / {$this->component_key}\n";

    // Create o/p directories, if necessary
    $r = file_prepare_directory($this->output_path, FILE_CREATE_DIRECTORY);
    foreach($this->output_map as $tplkey => $op_path) {
      $op_dir = $this->output_path . '/' . preg_replace('/\/[^\/]*$/', '', $op_path);
      $op_path = $this->output_path . '/' . $op_path;
      file_prepare_directory($op_dir, FILE_CREATE_DIRECTORY);
      $data = $this->templates_output[$tplkey];
      file_put_contents($op_path, $data);
    }

    // Recurse child components
    foreach($this->getChildComponents() as $child) {
      $child->write();
    }    

  }

  /**
   * Populate $tokens/replacements array
   * @return  None
   * Note that token values may also be set implicitly, via calls to 
   * getTokenValue() in cases where one token's replacement depends upon another's
   * @see  CodeComponent::tokens
   * @see  CodeComponent::generateTokenReplacement
   */
  protected function generateTokenReplacements() {
    // Only do this once (this function is executed recursively, but is called
    // in build(), which is also executed recursively)
    if($this->state['token_replacements_generated']) {
      return;
    } else {
      $this->state['token_replacements_generated'] = true;
    }
    if($this->state['dbgop']) print "generateTokenReplacements() - {$this->type} / {$this->component_key}\n";

    // Generate replacements
    foreach($this->token_keys as $token) {
      $this->generateTokenReplacement($token);
    }

    // Recurse child components
    foreach($this->getChildComponents() as $child) {
      $child->generateTokenReplacements();
    }
  }

  /**
   * Manage the process of generating the replacement text for a token
   * @param string $token token (key from $tokens array).
   * This function "wraps" the actual "set token value" logic, which is
   * performed in _generateTokenReplacement(). This indirection allows us to trap and
   * respond appropriately to circular dependencies among tokens.
   * (Note the token derivation logic allows for arbitrary dependencies among
   * components/tokens (tokens may refer to other tokens, including tokens
   * in other components). This makes for a flexible system but also makes it
   * relatively easy to inadvertently introduce circular references.)
   */
  protected function generateTokenReplacement($token) {

    // keep track of calls in order to handle circular references
    static $stack = array();
    if(in_array($token, $stack)) {
      throw new \Exception('Circular token reference: '. $token);
    }
    array_unshift($stack, $token);

    // Generate the replacemen value
    $this->_generateTokenReplacement($token);

    // remove token from call stack
    array_shift($stack);

  }

  /**
   * Generate replacement text for a token
   * @param string $token token (key from $tokens array).
   * Implmenting classes must implement this function.
   */
  abstract protected function _generateTokenReplacement($token);

  /**
   * Performs template token replacements
   * @return None
   */
  protected function expandTemplateTokens() {

  }

  protected function processTemplates() {
    if($this->state['dbgop']) print "processTemplates() - {$this->type} / {$this->component_key}\n";
    $tokens = $this->getAllAppliedTokens();
    foreach(array_keys($tokens) as $token) $tags[] = '{{' . $token . '}}';
    $this->templates_output = str_replace($tags, array_values($tokens), $this->templates_raw);

    // // Recurse child components
    // foreach($this->getChildComponents() as $child) {
    //   $child->processTemplates();
    // }
  }


  public function setParent($parent) {
    $this->parent = $parent;
  }

  // public function addChildComponent($component = null, $key = null, $type = null){
  // public function addChildComponent($config, $type, $class) {
  //   print_r(array($config, $type, $class));
  //   $component = new $class($cfg);

  //   $this->children[$type] = $component;
  //   print_r($this->children);
  //   die('RRRRRRRRRRR' . "\n");
  // }

  public function addChildComponents($config) {
    $types = $this->getComponentTypes();
    foreach($config as $type => $type_children) {
      foreach($type_children as $childconfig) {
        $class = $types['children'][$type];
        $component_config = array(
          'base_template_path' => $this->base_template_path,
          'schema' => $childconfig,
          'output_path' => '???',
          'owner' => $this,
        );
        $component = new $class($component_config);
        $this->children[$type][] = $component;
      }
    }
  }

  protected function loadTemplates() {
    if($this->state['dbgop']) print "loadTemplates() - {$this->type} / {$this->component_key}\n";
    foreach($this->template_map as $tplkey => $tplpath) {
      $this->templates_raw[$tplkey] = file_get_contents($this->base_template_path . $tplpath);
    }
  }

  protected function getComponentTypes() {
    if(!$this->owner) {
      return $this->component_types;
    } else {
      return $this->owner->getComponentTypes();
    }
  }
  /**
   * Returns a flat array of child CodeComponent object references
   * @return [type] [description]
   */
  protected function getChildComponents() {
    $children = array();
    foreach($this->children as $type => $component_type_children) {
      $children += $component_type_children;
    }
    // print_r($children);
    // die("555555555555555\n");
    return $children;
  }

  /**
   * Allow controller to set path based on derived configuration.
   * @param [type] $path [description]
   */
  public function setOutputPath($path) {
    $this->output_path = $path;
    foreach($this->getChildComponents() as $child) $child->setOutputPath($path);
  }

  /** 
   * Returns all token/replacement pairs for this component and its ancestors.
   * @return [type] [description]
   */
  protected function getAllAppliedTokens() {
    $tokens = array();
    if(is_object($this->owner)) {
      $tokens = array_merge($tokens, $this->owner->getAllAppliedTokens());
    }
    $tokens = array_merge($tokens, $this->tokens);
    return $tokens;
  }
}
