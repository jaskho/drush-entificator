<?php
namespace Drupal\entificator;

class EntificatorDefaultImporter extends EntificatorSchemaImporter
{

  /**
   * Import schema
   * @param  Varies $data_source Schema data or data source descriptor, as required by implementing
   *         class
   * @return None
   */
  public function import($configuration_data_source) {
    parent::import($configuration_data_source);
  }

  protected function load() {
    $this->schema_data = file_get_contents($this->configuration_data_source);
  }

  /**
   * Create $this->schema Entificator schema array from config data
   * @return None
   */
  protected function parse() {
    // $importer = $this->schema_importers[$this->scheme];
    // $this->importer = new $importer['class']($this->module_code_component_class, $this->entity_code_component_class);

    // Regexes for data extraction
    $patt_line_type = '/(.*)(\[([^\]]*)\]|)[ \t]*=/U';
    $patt_other_line_type = '/(.*)[ \t]*=/U';
    $patt_line_type = '/(.*)(\[([^\]]*)\]|)[ \t]*=/U';
    $patt_line_data = '/=[ \t]*(.*)/';
    $patt_other_line = '/^other\./';

    $data = $this->schema_data;

    // Remove blank lines and comments
    $data = preg_replace('/^[ \t]*#.*$/m', '', $data);
    $data = preg_replace('/^[ \t]*\n/m', '', $data );

    // Convert to array
    $data = explode("\n", $data);

    $datatemp = array(
      'module' => array(),
      'entities' => array(
      ),
    );

    foreach($data as $linekey => $line) {

      $typetmp = $type = $subtype = '';

      // Handle "other" config lines
      if(preg_match($patt_other_line, $line)) {
        preg_match_all($patt_other_line_type, $line, $matches);
        $typetmp = trim($matches[1][0], "\t\n\r\0\x0B");
        $typetmp = explode('.', $typetmp);
        $type = array_shift($typetmp);
        $subtype = $typetmp;

        preg_match_all($patt_line_data, $line, $matches);
        $info = trim($matches[1][0]);


      // Handle normal config line
      } else {
        preg_match_all($patt_line_type, $line, $matches);

        // Line "type" (eg., index, property, <other>)
        $type = trim($matches[1][0], "\t\n\r\0\x0B");
        if(strstr($type, '.')) {
          $typetmp = explode('.', $type);
          // if(count($typetemp) == 0) {
            $type = $typetmp[0];
            $subtype = $typetmp[1];
          // } 
        }
        $item_key = trim($matches[3][0]);
        // // Ignore blank lines
        // if(!$matches[0]) continue;

        preg_match_all($patt_line_data, $line, $matches);
        $info = trim($matches[1][0]);

        // Start processing a new entity
        if($type == 'entity' && $subtype == 'machine_name') {
          $ent_mach_name = strtolower(preg_replace('/[^a-zA-Z0-9_]/', "_", $info));
          $datatemp['entities'][$ent_mach_name] = array(
            'schema' => array(),
            'entity_info' => array(),
            'property_info' => array(),
            'bundles' => array(),
            'fields' => array(),
            'views_data' => array(
               'table' => array(),
             ),
            'ui' => array(),
          );
          $entdef = &$datatemp['entities'][$ent_mach_name];
        }

      }
      switch ($type) {
        case 'module':
          if($subtype) {
            $datatemp[$type][$subtype] = $info;
          } else {
            $datatemp[$type] = $info;
          }
         break;
        case 'schema':
          if($subtype == 'primary_key') {
            $info = explode(',', $info);
          }
          $entdef['schema'][$subtype] = $info;
          break;
        case 'index':
          $entdef['schema']['indexes'][$item_key] = explode(',', $info);
          break;
        case 'column':
          $colitem = $this->parseMultiSpec($info);
          $entdef['schema']['columns'][$item_key] = $this->parseColumnSpec($colitem);
          break;
        case 'entity':
          $entdef['entity_info'][$subtype] = $info;
          break;
        case 'bundle':
          break;
        case 'property':
          $fldtemp = array();
          $propitem = $this->parseMultiSpec($info);
          $propitem = $this->parsePropertySpec($propitem);
          $entdef['property_info'][$item_key] = $propitem['prop'];
          $entdef['views_data'][$item_key] = $propitem['view'];
          break;
        case 'views_data':
          $entdef['views_data']['table'][$subtype] = $info;
          break;
        case 'ui':
          $entdef['ui'][$subtype] = $info;
          break;
        case 'other':
          $other_item = &$datatemp;
          while ($subkeytmp = array_shift($subtype)) {
            if(!array_key_exists($subkeytmp, $other_item)) {
              $other_item[$subkeytmp] = array();
            }
            $other_item = &$other_item[$subkeytmp];
          } 
          $other_item = $info;
          break;
        default:
          if($subtype) {
            $datatemp[$type][$subtype] = $info;
          } else {
            $datatemp[$type] = $info;
          }
      }
      
    }
    // print_r($datatemp);die("gggggggggggg\n");
    $this->schema = $datatemp;

  }

  private function parseMultiSpec($info) {
    $fldtemp = array();
    // Encode escaped delimiters embedded in string data
    str_replace('\\,', '&#59;', $info);
    $fld = explode(',', $info);
    foreach($fld as $fldinfo) {
      $fldinfo = explode(':', $fldinfo);
      // Clean up
      $fldinfo[0] = trim($fldinfo[0]);
      $fldinfo[1] = trim($fldinfo[1]);
      // Replace escaped delimiters
      $fldinfo[1] = str_replace('&#59;', ',', $fldinfo[1]);
      $fldtemp[$fldinfo[0]] = $fldinfo[1];
    }
    return $fldtemp;
  }

  private function parseColumnSpec($colspec) {
    foreach($colspec as $specitemkey => &$specitemval) {
      // Handle boolean vals
      switch ($specitemkey) {
        case 'unsigned':
        case 'not null':
          // Anything except 'false' (case-insensitive), 0, '' or null
          // evaluates as true
          $istrue = (bool) $specitemval;
          $istrue = $istrue && ! preg_match('/^false$/i', $specitemval);
          $specitemval = $istrue;
          break;
      }
    }
    return $colspec;
  }

  private function parsePropertySpec($propspec) {
    static $viewskeypatt = '/^views\./';
    $return = array('prop' => array(), 'view' => array());
    foreach($propspec as $specitemkey => $specitemval) {
      if(preg_match($viewskeypatt, $specitemkey)) {
        $keytmp = preg_replace($viewskeypatt, '', $specitemkey); 
        $return['view'][$keytmp] = $specitemval;
      } else {
        $return['prop'][$specitemkey] = $specitemval;
      }
    }
    return $return;
  }

}