<?php
namespace Drupal\entificator;
use Drupal\builder\CodeComponentBase as CodeComponentBase;

class EntityCodeComponent extends CodeComponentBase {

  public $type = 'entity';

  /**
   * CodeComponentBase::token_keys
   */
  protected $token_keys = array(
    'ENTITY_MACHINE_NAME',
    'ENTITY_HUMAN_NAME',
    'ENTITY_HUMAN_NAME_PLURAL',
    'SCHEMA_TABLE_NAME',
    // 'ENTITY_PK_FIELD',
    'ENTITY_LABEL_FIELD',
    'ENTITY_CLASS',
    'ENTITY_CONTROLLER_CLASS',
    'ENTITY_DEFAULT_URI_ROOT',
    // 'LIST_VIEW_PATH',
    // 'ENTITY_LIST_VIEW_MACHINE_NAME',
    'SCHEMA_TABLE_ARRAY',
    'SCHEMA_SUB_FUNC_NAME',
    'SCHEMA_SUB_CALL',
  );

  /**
   * Reference to owner 'module' component's schema['module'] array
   * @var ByRef (array)
   */
  private $module_schema = null;



  /**
   * Implements CodeComponent::__construct()
   */
  public function __construct($config = null) {
    $this->type = 'entity';
    $this->component_key = $config['schema']['entity_info']['machine_name'];
    parent::__construct($config);
    $this->module_schema = &$this->owner->getModuleSchemaRef();
    // ['module_schema'];
  } 

  /**
   * Implements CodeComponentBase::mapTemplates().
   * @return [type] [description]
   */
  protected function mapTemplates() {
    $template_map = array(

      // ### Module Files      
      'module/lib/Entity.php' => 'module/lib/%EntityName%.php',
      'module/lib/EntityController.php' => 'module/lib/%EntityName%Controller.php',
      'module/includes/%entity%.entity.inc' => 'module/includes/%entity_name%.entity.inc',
      'module/includes/%entity%_admin.php' => 'module/includes/%entity_name%.admin.php',

      // ### Partials
    
      // Schema API-related
      //   table array
      'partials/hook_schema/entity_schema_array.part' =>
        'partials/hook_schema/%entity_dir%entity_schema_array.part',
      //   hook_schema sub-function
      'partials/hook_schema/entity_schema_sub.part' =>
        'partials/hook_schema/%entity_dir%entity_schema_sub.part',
      //   call to hook_schema sub-function
      'partials/hook_schema/entity_schema_sub_call.part' =>
        'partials/hook_schema/%entity_dir%entity_schema_sub_call.part',


      // Entity API-related
      //   entity info array
      'partials/hook_entity_info/entity_info_array' =>
        'partials/hook_entity_info/%entity_dir%entity_info_array.part',
      //   hook_entity_info sub-function
      'partials/hook_entity_info/entity_info_sub.part' =>
        'partials/hook_entity_info/%entity_dir%entity_info_sub.part',
      //   call to hook_entity_info sub-function
      'partials/hook_entity_info/entity_info_sub_call.part' =>
        'partials/hook_entity_info/%entity_dir%entity_info_sub_call.part',


      // Hook_permissions-related
      //   entity perms array
      'partials/hook_permission/entity_perm_array' =>
        'partials/hook_permission/%entity_dir%entity_perm_array.part',
      //   hook_permissions sub-function
      'partials/phook_permission/ntity_perm_sub' =>
        'partials/hook_permission/%entity_dir%entity_perm_sub.part',
      //   call to hook_permissions sub-function
      'partials/hook_permission/entity_perm_sub_call' =>
        'partials/hook_permission/%entity_dir%entity_perm_sub_call.part',


      // Hook_menu-related
      //   entity menu items array
      'partials/hook_menu/entity_menu_items_array.part' =>
        'partials/hook_menu/%entity_dir%entity_menu_items_array.part',
      //   call to hook_menu sub-function
      'partials/hook_menu/entity_menu_sub' =>
        'partials/hook_menu/%entity_dir%entity_menu_sub.part',
      //   hook_menu sub-function
      'partials/hook_menu/entity_menu_sub_call' =>
        'partials/hook_menu/%entity_dir%entity_menu_sub_call.part',


      // hook_views_data-related
      //   entity menu items array
      'partials/hook_views_data/entity_views_data_array.part' =>
        'partials/hook_views_data/%entity_dir%entity_views_data_array.part',
      //   call to hook_views_data sub-function
      'partials/hook_views_data/entity_views_data_sub' =>
        'partials/hook_views_data/%entity_dir%entity_views_data_sub.part',
      //   hook_views_data sub-function
      'partials/hook_views_data/entity_views_data_sub_call' =>
        'partials/hook_views_data/%entity_dir%entity_views_data_sub_call.part',


      // hook_views_default_views-related
      //   entity menu items array
      'partials/hook_views_default_views/entity_default_views_array.part' =>
        'partials/hook_views_default_views/%entity_dir%entity_default_views_array.part',
      //   call to hook_views_default_views sub-function
      'partials/hook_views_default_views/entity_default_views_sub' =>
        'partials/hook_views_default_views/%entity_dir%entity_default_views_sub.part',
      //   hook_views_default_views sub-function
      'partials/hook_views_default_views/entity_default_views_sub_call' =>
        'partials/hook_views_default_views/%entity_dir%entity_default_views_sub_call.part',


    );

    $replacements = array(
      'template_map' => array(
        '%entity_dir%' => '',
      ),
      'output_map' => array(
        '%EntityName%' => $this->schema['entity_info']['camel_case'],
        '%entity_name%' => $this->schema['entity_info']['machine_name'],
        '%entity_dir%' => $this->schema['entity_info']['machine_name'] . '/',
      ),
    );
    parent::mapTemplates($template_map, $replacements);
  }

  /**
   * Implements CodeComponentBase::validate().
   * We're breaking the contract here, throwing exceptions rather than
   * returning a validation flag/error description.
   */
  protected function validate($config) {

    parent::validate($config);

    $err = null;

    if(!$config) $err = 'Configuration not provided';

    if(!$err && !$config['module_schema']) 
          $err = 'Module configuration reference is required';

    if(!$err && !$config['schema']['entity_info']['machine_name']) 
          $err = 'Entity machine name is required';

    // If we're inferring the entity PK, we need a single pk column
    if(!$err && !$config['schema']['entity_info']['id_key']) {
      if(count($entity['schema']['schema']['primary_key']) > 1) {
        $err = 'Cannot infer Entity ID Field (Table has multi-field PK)';
      } 
    }

    if($err) {
      throw new \Exception('Invalid entity component configuration: ' . $err);
    }

    return CodeComponentBase::VALIDATION_PASSED;

  }

  /**
   * Apply default values
   * @return None 
   * Note: If this is the first entity being processed, this function will
   *       also generate (if necessary) entity-based module defaults (ie.
   *       module_name and description).  Doing it here minimizes the 
   *       spaghetti that results from the two-way dependency between entity
   *       and module values.
   */
  protected function preprocess() {

    $entity = &$this->schema;
    $module = &$this->owner->getModuleSchemaRef();

    // ###  Names and descriptions  ###

    // Everything flows from the entity machine name
    $ent_mach_name = $entity['entity_info']['machine_name'];


    // ###  Entity items (not dependent on module name)
    // Plural machine name
    if(!$entity['entity_info']['machine_name_plural']) {
      $entity['entity_info']['machine_name_plural'] = $ent_mach_name . 's';
    }
    // Human name
    if(!$entity['entity_info']['human_name']) {
      $entity['entity_info']['human_name'] = ucwords(str_replace('_', ' ', $ent_mach_name));
    } 
    $ent_human_name = $entity['entity_info']['human_name'];
    // Plural human name
    if(!$entity['entity_info']['human_name_plural']) {
      $entity['entity_info']['human_name_plural'] = $ent_human_name . 's';
    }
    // Camel case name
    if(!$entity['entity_info']['camel_case']) {
      $entity['entity_info']['camel_case'] = str_replace(' ', '', $ent_human_name);
    }
    // ###  Module items (dependent on entity name)
    // Machine name
    if(!$module['machine_name']) {
      $module['machine_name'] = $ent_mach_name . '_model';
    }
    $module_machine_name = $module['machine_name'];
    // Human name
    if(!$module['human_name']) {
      $module['human_name'] = ucwords(str_replace('_', ' ', $module_machine_name));
    }
    $module_human_name = $module['human_name'];
    // Description 
    if(!$module['description']) {
      $module['description'] = $ent_human_name . " entity module auto-generated by Entificator";
    }

    // ###  Entity-level default values
    // Schema
    //   Fieldable
    if(!$entity['entity_info']['fieldable'])
          $entity['entity_info']['fieldable'] = false;
    //   Uses Bundles
    if(!$entity['entity_info']['uses_bundles'])
          $entity['entity_info']['uses_bundles'] = false;
    //   Auto PK field name
    if(!$entity['schema']['auto_pk_name'])
          $entity['schema']['auto_pk_name'] = 'id_0';
    //   Table PK field
    //   Supply one if necessary.
    if(!$entity['schema']['primary_key']) {
      $pk_col = array(
        $entity['schema']['auto_pk_name'] => array(
          'type' => 'serial',
          'unsigned' => true,
          'not null' => true,
          'description' => 'Auto-num pk field added by Entificator',
        ),
      );
      $entity['schema']['columns'] = $pk_col + $entity['schema']['columns'];
      $entity['schema']['primary_key'] = array($entity['schema']['auto_pk_name']);
    }
    //   Entity PK field
    if(!$entity['entity_info']['id_key']) {
      // We've already ensured we don't have a multi-field table pk in validate()
      $entity['entity_info']['id_key'] = array_shift(array_slice($entity['schema']['primary_key'], 0, 1));
    }
    if(!$entity['entity_info']['label_key'])
          $entity['entity_info']['label_key'] = $entity['entity_info']['id_key'];
    if(!$entity['entity_info']['bundle_key'])
          $entity['entity_info']['bundle_key'] = false;
    if(!$entity['entity_info']['revision_key'])
          $entity['entity_info']['revision_key'] = false;
    if(!$entity['entity_info']['language_key'])
          $entity['entity_info']['language_key'] = false;
    // if(!$entity['entity']['view_modes'])
    //       $entity['entity']['view_modes'] = false;
    $entity['entity_info']['view_modes'] = array(
      'label' => t('Default'),
      'custom settings' => false,
    );



    // Entity-level Views Data config
    if(!$entity['views_data']['table']['machine_name'])
          $entity['views_data']['table']['machine_name'] = $entity['entity_info']['machine_name_plural'];
    if(!$entity['views_data']['table']['table_group'])
          $entity['views_data']['table']['table_group'] = $module_human_name;

    // UI
    //   Admin
    if(!$entity['ui']['admin_root_path'])
          $entity['ui']['admin_root_path'] = 'admin/' . $module['machine_name'] . '/' . $entity['entity_info']['machine_name'];
    $admin_root = $entity['ui']['admin_root_path'];
    if(!$entity['ui']['admin_list_items_path'])
          $entity['ui']['admin_list_items_path'] = '%ADMIN_ROOT_PATH%/list';
    if(!$entity['ui']['admin_add_item_path'])
          $entity['ui']['admin_add_item_path'] = '%ADMIN_ROOT_PATH%/add';
    if(!$entity['ui']['admin_view_item_path'])
          $entity['ui']['admin_view_item_path'] = '%ADMIN_ROOT_PATH%/%ENTITY_ID%';
    if(!$entity['ui']['admin_edit_item_path'])
          $entity['ui']['admin_edit_item_path'] = '%ADMIN_ROOT_PATH%/%ENTITY_ID%/edit';
    if(!$entity['ui']['admin_delete_item_path'])
          $entity['ui']['admin_delete_item_path'] = '%ADMIN_ROOT_PATH%/%ENTITY_ID%/delete';
    //   Public


    // Process path tokens
    $path_keys = array(
      'admin_root_path', 
      'admin_list_items_path', 
      'admin_add_item_path', 
      'admin_view_item_path', 
      'admin_edit_item_path', 
      'admin_delete_item_path', );
    $path_tokens = array(
      '%ADMIN_ROOT_PATH%',
      '%MODULE_MACAME%',
      '%ENTITY_MACHINE',
      '%ENTITY_ID%',
     );
     $path_replacements = array(
      $entity['ui']['admin_root_path'],
      $module['machine_name'],
      $entity['entity_info']['machine_name'],
      '%' . $entity['entity_info']['machine_name'],
    );    
    foreach($path_keys as $path_key) {
      $entity['ui'][$path_key] = str_replace($path_tokens, $path_replacements, $entity['ui'][$path_key]);
    }

    if(!$entity['ui']['entity_default_uri_root'])
          $entity['ui']['entity_default_uri_root'] = $ent_mach_name;


    // Column/Property defaults

    //   Ensure there's a matching property for each column (the reverse does
    //   not need to be true--not every property maps to a field).
    $entity['property_info'] = $entity['property_info'] + array_fill_keys(array_keys($entity['schema']['columns']), array());

    foreach($entity['property_info'] as $propkey => &$propdef) {
      unset($coldef, $viewspropdef);
      // Get reference to schema column array item (there may not be one)
      if(array_key_exists($propkey, $entity['schema']['columns'])) {
        $coldef = &$entity['schema']['columns'][$propkey];
      }
      // Get reference to entity property array item
      $viewspropdef = &$entity['views_data'][$propkey];

      // Column defaults for Schema API
      if($coldef) {
        if(!$coldef['type'])
              $coldef['type'] = 'varchar';
        if(!$coldef['unsigned']) {
          if($coldef['type'] == 'serial')
            $coldef['unsigned'] = false;
        }
        if(!$coldef['not null']) {
          if($coldef['type'] == 'serial')
            $coldef['not null'] = true;
        }
        if(!$coldef['length']) {
          if($coldef['type'] == 'varchar')
            $coldef['length'] = 100;
        }
        if(!$coldef['size']) $coldef['size'] = 'normal';
      }

      // Property defaults for Entity API
      if(!$propdef['label'])
            $propdef['label'] = '';
      if(!$propdef['description'])
            $propdef['description'] = '';
      if(!$propdef['type']) {
        switch($coldef['type']) {
          case 'serial':
          case 'int':
          case 'float':
          case 'numeric':
            $propdef['type'] = 'integer';
            break;
          case 'varchar':
          case 'char':
          case 'text':
          case 'blob':
            $propdef['type'] = 'text';
            break;
          case 'datetime':
            throw new \Exception('Schema API column type \'datetime\' is not supported');
          default:

        }
      }

      // Property defaults for Views API 
      $viewpropdeftemp = array();
      if(!$viewspropdef['title'])
            $viewspropdef['title'] = $entity['entity_human_name_plural'];

      if(!$viewspropdef['help'])
            $viewspropdef['help'] = '';

      if(!$viewspropdef['field.title'])
            $viewspropdef['field.title'] = $fldkey;


      // Views handlers
      // Use handler most approriate to the underlying property type
      //   Field
      if(!$viewspropdef['field.handler']) {
        switch($propdef['type']) {
          case 'text':
          case 'token':
            $viewspropdef['field.handler'] = 'views_handler_field';
            break;
          case 'uri':
            $viewspropdef['field.handler'] = 'views_handler_field_link';
            break;
          case 'integer':
          case 'decimal':
          case 'duration':
            $viewspropdef['field.handler'] = 'views_handler_field_numeric';
            break;
          case '{entities}':
          case 'entity':
            $viewspropdef['field.handler'] = 'views_handler_field_entity';
            break;
          case 'boolean':
            $viewspropdef['field.handler'] = 'views_handler_field_boolean';
            break;
          case 'date':
            $viewspropdef['field.handler'] = 'views_handler_field_date';
            break;
        }
      }
      //   Filter
      if(!$viewspropdef['filter.handler']) {
        switch($propdef['type']) {
          case 'text':
          case 'token':
          case 'uri':
            $viewspropdef['filter.handler'] = 'views_handler_filter_string';
            break;
          case 'integer':
          case 'decimal':
          case 'duration':
          case '{entities}':
          case 'entity':
            $viewspropdef['filter.handler'] = 'views_handler_filter_numeric';
            break;
          case 'boolean':
            $viewspropdef['filter.handler'] = 'views_handler_filter_boolean';
            break;
          case 'date':
            $viewspropdef['filter.handler'] = 'views_handler_filter_date';
            break;
        }
      }
      //   Sort
      if(!$viewspropdef['sort.handler']) {
        switch($propdef['type']) {
          case 'text':
          case 'token':
          case 'uri':
          case 'integer':
          case 'decimal':
          case 'duration':
          case '{entities}':
          case 'entity':
          case 'boolean':
            $viewspropdef['sort.handler'] = 'views_handler_sort';
            break;
          case 'date':
            $viewspropdef['sort.handler'] = 'views_handler_sort_date';
            break;
        }
      }



    } # /$entity['property_info']
  }

  protected function postprocess() {

    $entity = &$this->schema;
    $module = $this->owner->getModuleSchemaRef();
    $ent_mach_name = $entity['entity_info']['machine_name'];
    $ent_human_name = $entity['entity_info']['human_name'];
    $module_machine_name = $module['machine_name'];
    $module_human_name = $module['human_name'];

    // ###  Entity items (dependent on module name)
    // Table name
    if(!$entity['schema']['table_name']) {
      $entity['schema']['table_name'] = $module_machine_name . '_' . $ent_mach_name;;
    }
    // Table description
    if(!$entity['schema']['table_description']) {
      $entity['schema']['table_description'] = $module_human_name . ' module ' . $ent_human_name . ' data';
    }
    parent::postprocess();    
  }

  /**
   * Implements CodeComponentBase::_generateTokenReplacement().
   */
  protected function _generateTokenReplacement($token) {

    $schema = $this->schema;

    switch($token) {

      // Entity general
      case 'ENTITY_MACHINE_NAME':
        $this->tokens[$token] = $schema['entity_info']['machine_name'];
        break;
      
      // Entity schema
      case 'SCHEMA_TABLE_NAME':
        $this->tokens[$token] = $schema['schema']['table_name'];
        break;

      // Entity info
      case 'ENTITY_HUMAN_NAME':
        $this->tokens[$token] = $schema['entity_info']['human_name'];
        break;
      case 'ENTITY_HUMAN_NAME_PLURAL':
        $this->tokens[$token] = $schema['entity_info']['human_name_plural'];
        break;
      case 'ENTITY_PK_FIELD':
        $this->tokens[$token] = $schema['entity_info']['id_key'];
        break;
      case 'ENTITY_LABEL_FIELD':
        $this->tokens[$token] = $schema['entity_info']['label_key'];
        break;
      case 'ENTITY_CLASS':
        $this->tokens[$token] = $schema['entity_info']['camel_case'];
        break;
      case 'ENTITY_CONTROLLER_CLASS':
        $this->tokens[$token] = $schema['entity_info']['camel_case'] . 'Controller';
        break;
      case 'ENTITY_DEFAULT_URI_ROOT':
        $this->tokens[$token] = $schema['ui']['entity_default_uri_root'];
        break;
      
      case 'LIST_VIEW_PATH':
        $this->tokens[$token] = $schema['list_view_path'];
        break;
      case 'VIEWS_TABLE_GROUP':
        $this->tokens[$token] = $schema['views_table_group'];
        break;

      case 'ENTITY_LIST_VIEW_MACHINE_NAME':
        $this->tokens[$token] = $schema['view_machine_name'];
        break;
    
      // Entity code output
      case 'SCHEMA_TABLE_ARRAY':
        $this->tokens[$token] = $this->buildSchemaTableArray();
        break;

      // Views


      // Entity other
      case 'SCHEMA_SUB_FUNC_NAME':
        $this->tokens[$token] = $this->buildSchemaSubName();
        break;
      case 'SCHEMA_SUB_CALL':
        $this->tokens[$token] = $this->buildSchemaSubCall();
        break;

    }

    // return $this->tokens[$token];
  }

  private function buildSchemaTableArray() {

    $schema = $this->schema;

    // PKs array
    $pks_array = '';
    foreach($schema['schema']['primary_key'] as $pk_field) {
      $pks_array .= "    '{$pk_field}',\n";
    }

    // Indexes array
    $indexes_array = '';
    if($schema['schema']['indexes']) {
      foreach($schema['schema']['indexes'] as $indexkey => $indexflds) {
        $idxtmp = "    '{$indexkey}' => '" . implode('\',\'', $indexflds) . "',\n";
        $indexes_array .= $idxtmp;
      }
    }

    // Fields array
    $fields_array = '';
    foreach($schema['schema']['columns'] as $fldkey => $flddef) {
      $fields_array .= "    '{$fldkey}' => array(\n";
      // Field array items
      $schema_api_field_array_keys = array(
        'type', 'unsigned', 'not null', 'length', 'description', 'size',
      );
      foreach($schema_api_field_array_keys as $field_array_key) {
        $field_array_val = $flddef[$field_array_key];
        // if(is_string($flddefval)) {
        switch($field_array_key) {
          case 'type':
          case 'size':
          case 'description':
            $tmpflddefval = "'{$field_array_val}'";;
            break;
          case 'unsigned':
          case 'not null':
           $tmpflddefval = $field_array_val ? 'true' : 'false';
            break;
          case 'length':
            $tmpflddefval = $field_array_val ? $field_array_val : 'null';
            break;
        }
        $fields_array .= "      '{$field_array_key}' => {$tmpflddefval},\n";
      }
      $fields_array .= "    ),\n";
    }
    // Table array
    $table_array .= "  'description' => '" . $schema['schema']['description'] . "',\n";
    $table_array .= "  'primary key' => array(\n";
    $table_array .= $pks_array;
    $table_array .= "   ),\n";
    $table_array .= "  'indexes' => array(\n";
    $table_array .= $indexes_array;
    $table_array .= "  ),\n";
    $table_array .= "  'fields' => array(\n";
    $table_array .= $fields_array;
    $table_array .= "  ),\n";

    $schema_array = "\$schema['{$schema['schema']['table_name']}'] = array(\n";
    $schema_array .= $table_array;
    $schema_array .= ");\n";

    return $schema_array;
  }

  
  private function buildSchemaSubName() {
    $module = $this->owner->getModuleSchemaRef();
    $mod_mach_name = $module['machine_name'];
    $ent_mach_name = $this->schema['entity_info']['machine_name'];
    return $mod_mach_name . '_' . $ent_mach_name . '_schema';
  }


  private function buildSchemaSubCall() {
    $fname = $this->getTokenValue('SCHEMA_SUB_FUNC_NAME');
    $fcall = "\$schema += $fname();";
    return $fcall;
  }

  /**
   * List files for module.info
   * @return array of generated filepaths
   */
  public function getOutputFilesList() {
    $module_file_keys = array(
      'module/lib/Entity.php',
      'module/lib/EntityController.php',
      'module/includes/%entity%.entity.inc',
      'module/includes/%entity%_admin.php',
    );
    $module_files = array_fill_keys($module_file_keys, null);
    return array_values(array_intersect_key($this->output_map, $module_files));
  }

  private function buildViewsDataArray() {
    $schema = $this->importer->schema;
    // Views data array field arrays
    $fldstmp = '';
    $views_data_key = $schema['module_machine_name'] . '_' . $schema['entity_machine_name'];
    foreach($schema['properties'] as $fldkey => $flddef) {
      $fldtmp = '';
      $fldtmp .= "    \$data['{$views_data_key}']['{$fldkey}'] = array(\n";
      $fldtmp .= "      'field' => array(\n";
      $fldtmp .= "        'title' => '{$flddef['views.field.title']}',\n";
      $fldtmp .= "        'handler' => '{$flddef['views.field.handler']}',\n";
      $fldtmp .= "        'click sortable' => {$flddef['views.field.click sortable']},\n";
      $fldtmp .= "       ),\n";
      $fldtmp .= "       'filter' => array(\n";
      $fldtmp .= "        'handler' => '{$flddef['filter.field.handler']}',\n";
      $fldtmp .= "       ),\n";
      $fldtmp .= "       'sort' => array(\n";
      $fldtmp .= "        'handler' => '{$flddef['views.sort.handler']}',\n";
      $fldtmp .= "       ),\n";
      $fldtmp .= "     );\n";
      $fldstmp .= $fldtmp;
    }
    $replacements['TAG_VIEWS_DATA_ARRAY_FIELD_ARRAYS'] = $fldstmp;

    // TAG_ENTITY_VIEWS_DATA_ARRAY
    $datatmp = '';
    $datatmp .= "    \$data['']['table']['group'] = {}\n";
    $datatmp .= "    \$data['']['table']['base'] = array(\n";
    $datatmp .= "      'title' => '{}',\n";
    $datatmp .= "      'field' => '{}',\n";
    $datatmp .= "      'label' => '{}',\n";
    $datatmp .= "    );\n";
    $replacements['TAG_ENTITY_VIEWS_DATA_ARRAY'] = $datatmp;
    $replacements['TAG_ENTITY_VIEWS_DATA_ARRAY'] .= $replacements['TAG_VIEWS_DATA_ARRAY_FIELD_ARRAYS'];

    return $datatmp . $fldstmp;
  }
  




  private function buildSchemaSubBody($ent_mach_name) {
    $fname = $this->replacements['entities'][$ent_mach_name]['SCHEMA_SUB_FUNC_NAME'];
    $table_array = $this->replacements['entities'][$ent_mach_name]['SCHEMA_TABLE_ARRAY'];
    $fbody = "function {$fname}() {\n";
    $fbody .= $table_array;
    $fbody .= "  return \$schema;\n";
    $fbody .= "}\n\n";
    return $fbody;
  }


}
