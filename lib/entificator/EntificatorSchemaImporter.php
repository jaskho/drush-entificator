<?php
namespace Drupal\entificator;

abstract class EntificatorSchemaImporter
{
 
  /**
   * {Entificator Schema} array
   * See documentation in entificator.drush.inc
   * @var Array
   */
  public $schema = null;

  protected $configuration_data_source = '';

  protected $schema_data = '';

  /**
   * Import schema
   * @param  Varies $data_source 
   *         Schema data or data source descriptor, as required by implementing
   *         class
   * @return None
   * Entry point to executing import operation.
   */
  public function import($configuration_data_source) {
    $this->configuration_data_source = $configuration_data_source;
    $this->load();
    $this->parse();

  }

  abstract protected function load();

  abstract protected function parse();

}