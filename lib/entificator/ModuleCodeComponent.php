<?php
namespace Drupal\entificator;
use Drupal\builder\CodeComponentBase as CodeComponentBase;

class ModuleCodeComponent extends CodeComponentBase {

  public $type = 'module';

  /**
   * CodeComponentBase::token_keys
   */
  protected $token_keys = array(
    'MODULE_MACHINE_NAME',
    'MODULE_NAME',
    'MODULE_DESCRIPTION',
    'MODULE_PACKAGE',
    'MODULE_VERSION',
    'MODULE_DEPENDENCIES',
    'FILE_DEPENDENCIES',
    'DRUPAL_CORE_VERSION',
    'SCHEMA_SUB_CALLS',
  );


  protected $output_path = null;

  /**
   * CodeComponentBase::component_types
   */
  protected $component_types = array(
    'app' => 'Drupal\entificator\ModuleCodeComponent',
    'children' => array(
      'entity' => 'Drupal\entificator\EntityCodeComponent',
    ),
  );


  /**
   * Entity child component classes
   */
  private $entity_code_component_class = 'Drupal\entificator\EntityCodeComponent';


  /**
   * Implements CodeComponent::__construct()
   */
  public function __construct($config = null) {
    $this->type = 'module';
    $this->component_key = 'module';
    parent::__construct($config);

    // Instantiate child (entity) components
    // Note: the first entity will (if necessary) set the module's entity-
    //       based defaults (module name & description)
    try {
      $this->addChildComponents(array('entity' => $this->schema['entities'])) ;
    } catch (\Exception $e) {
      // Raise up the chain
      throw $e;
    }
  } 

  /**
   * Implements CodeComponentBase::mapTemplates().
   * @return [type] [description]
   */
  protected function mapTemplates() {
    $template_map = array(


      // ### Module Files
      'module/module.module' => 'module/%module_name%.module',
      'module/module.info' => 'module/%module_name%.info',
      'module/module.install' => 'module/%module_name%.install',
      'module/module.api.php' => 'module/%module_name%.api.php',


      'module/views/module.views_default.inc' => 'module/views/%module_name%.views_default.inc',
      'module/views/module.views.inc' => 'module/views/%module_name%.views.inc',

      // ### Partials
    
      // Schema API-related
      //   hook_schema() function
      'partials/hook_schema/hook_schema_function.part' => 'partials/hook_schema/hook_schema_function.part',
      //   calls to per-entity hook_schema sub-functions (for adding to 
      //     existing hook_schema() fxn)
      'partials/hook_schema/hook_schema_sub_calls.part' => 'partials/hook_schema/hook_schema_sub_calls.part',

      // Entity API-related
      //   hook_entity_info() function
      'partials/hook_entity_info/hook_entity_info_function.part' => 'partials/hook_entity_info/hook_entity_info_function.part',
      //   calls to per-entity hook_entity_info sub-functions (for adding to 
      //     existing hook_entity_info() fxn)
      'partials/hook_entity_info/hook_entity_info_sub_calls.part' => 'partials/hook_entity_info/hook_entity_info_sub_calls.part',

      // Hook_permissions-related
      //   hook_permission() function
      'partials/hook_permission/hook_perm_function.part' => 'partials/hook_permission/hook_perm_function.part',
      //   calls to per-entity hook_permission sub-functions (for adding to 
      //     existing hook_permission() fxn)
      'partials/hook_permission/hook_perm_sub_calls.part' => 'partials/hook_permission/hook_perm_sub_calls.part',
    
      // Hook_menu-related
      // Module hooks-related
      //   hook_menu() function
      'partials/hook_menu/hook_menu_function.part' => 'partials/hook_menu/hook_menu_function.part',
      //   calls to per-entity hook_menu sub-functions (for adding to 
      //     existing hook_menu() fxn)
      'partials/hook_menu/hook_menu_sub_calls.part' => 'partials/hook_menu/hook_menu_sub_calls.part',

      // hook_views_data-related
      //   hook_views_data() function
      'partials/hook_views_data/hook_views_data_function.part' => 'partials/hook_views_data/hook_views_data_function.part',
      //   calls to per-entity hook_views_data sub-functions (for adding to 
      //     existing hook_views_data() fxn)
      'partials/hook_views_data/hook_views_data_sub_calls.part' => 'partials/hook_views_data/hook_views_data_sub_calls.part',

      // hook_views_default_views-related
      //   hook_views_default_views() function
      'partials/hook_views_default_views/hook_views_default_views_function.part' => 'partials/hook_views_default_views/hook_views_default_views_function.part',
      //   calls to per-entity hook_views_default_views sub-functions (for adding to 
      //     existing hook_views_default_views() fxn)
      'partials/hook_views_default_views/hook_views_default_views_sub_calls.part' => 'partials/hook_views_default_views/hook_views_default_views_sub_calls.part',

      
      // Other
      'partials/hook_views_api.part' => 'partials/hook_views_api.part',



    );
    // path/filename replacements
    $replacements = array();
    $replacements = array(
      'template_map' => array(
        '%module_name%' => 'module',
      ),
      'output_map' => array(
        '%module_name%' => $this->schema['module']['machine_name'],
      ),
    );
    parent::mapTemplates($template_map, $replacements);
  }

  /**
   * Implements CodeComponent::preprocess().
   */
  protected function preprocess() {

    $schema = &$this->schema;

    // NOTES:
    // + Entity-derived defaults are generated by child entity (see EntityCodeComponent::applyDefaults())
    //    + machine_name
    //    + human_name
    //    + description
    // + Child entities' defaults are applied at instantiation time.

    // Normal module config
    if(!$schema['module']['sites_dir'])
          $schema['module']['sites_dir'] = 'all';
    if(!$schema['module']['sub_dir'])
          $schema['module']['sub_dir'] = '';
    if(!$schema['module']['drupal_core_version'])
          $schema['module']['drupal_core_version'] = '7.x';
    if(!$schema['module']['package_name'])
          $schema['module']['package_name'] = 'Entificator';
    if(!$schema['module']['version'])
          $schema['module']['version'] = '7.x-1.0';

    // NOTE: file_dependencies is handled during token evaluation
    
    if(!$schema['module']['module_dependencies'])
          $schema['module']['module_dependencies'] = 'entity,views,views_ui';
  }

  /**
   * Implements CodeComponentBase::_generateTokenReplacement().
   */
  protected function _generateTokenReplacement($token) {

    $schema = $this->schema;
    switch($token) {
      case 'MODULE_MACHINE_NAME':
        $this->tokens[$token] = $schema['module']['machine_name'];
        break;
      case 'MODULE_NAME':
        $this->tokens[$token] = $schema['module']['human_name'];
        break;
      case 'MODULE_DESCRIPTION':
        $this->tokens[$token] = $schema['module']['description'];
        break;
      case 'MODULE_PACKAGE':
        $this->tokens[$token] = $schema['module']['package_name'];
        break;
      case 'MODULE_VERSION':
        $this->tokens[$token] = $schema['module']['version'];
        break;
      case 'MODULE_DEPENDENCIES':
        $this->tokens[$token] = $this->buildDotInfoModuleDependencies();
        break;
      case 'FILE_DEPENDENCIES':
        $this->tokens[$token] = $this->buildDotInfoFileDependencies();
        break;
      case 'DRUPAL_CORE_VERSION':
        $this->tokens[$token] = $schema['module']['drupal_core_version'];
        break;

      case 'SCHEMA_SUB_CALLS':
        $this->tokens[$token] = $this->buildSchemaSubCalls();
        break;
    }
  }



  public function &getModuleSchemaRef() {
    return $this->schema['module'];
  }

  public function getModuleMachineName() {
    return $this->schema['module']['machine_name'];
  }

  public function getModuleInstallDir() {
    $schema = $this->schema['module'];
    $rtrn = 'sites/' . $schema['sites_dir'] . '/modules';
    $rtrn .= $schema['sub_dir'] ? '/' . $schema['sub_dir'] : '';
    $rtrn .= '/' . $schema['machine_name'];
    return $rtrn;
  }



  private function buildSchemaSubCalls() {
    $fcalls = '';

    foreach($this->children['entity'] as $entity) {
      $fcall = $entity->getTokenValue('SCHEMA_SUB_CALL');
      $fcalls .= "  {$fcall}\n";
    }
    return $fcalls;
  }

  private function buildDotInfoModuleDependencies() {
    $mods = explode(',', $this->schema['module']['module_dependencies']);
    $op = '';
    foreach($mods as $mod) {
      $op .= "dependencies[] = $mod\n";
    }
    return $op;
  }  

  private function buildDotInfoFileDependencies() {
    $schema = &$this->schema;
    if(!$schema['module']['file_dependencies']) {
      // Get entity(s)'s file dependencies.
      $schema['module']['file_dependencies'] = '';
      foreach($this->children['entity'] as $entity) {
        foreach($entity->getOutputFilesList() as $file) {
          $schema['module']['file_dependencies'][] = $file;
        }
      }
      $schema['module']['file_dependencies'] = implode(',', $schema['module']['file_dependencies']);
    }


    $files = explode(',', $this->schema['module']['file_dependencies']);
    $files += explode(',', $this->schema['module']['additional_file_dependencies']);
    $op = '';
    foreach($files as $file) {
      $op .= "files[] = $file\n";
    }
    return $op;
  }  
}
