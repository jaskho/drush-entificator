<?php
namespace Drupal\entificator;

/**
 *  Output Files
 *  
 *  module
 *    {module files}
 *  partials
 *                + From template
 *                entity_info_array                 # add to hook_entity_info (if exists)
 *                entity_perms_function             # add to module.module or inc file (if not putting menu array directly in hook_perm)
 *                menu_entity_menu_function         # add to module.module or inc file (if not putting menu array directly in hook_menu)
 *                module_entity_api_hook_examples   # add to module.api.php
 *                schema_hook_schema                # add to hook_schema
 *                views_data_array                  # add to hook_views_data
 *                views_hook_views_api              # add (if nec) to module.module or inc file
 *                views_hook_views_default_views    # add to hook_views_default_views
 *
 *                + Other
 *                hook_menu                         # add to module.module or inc file (if hook_menu already implemented,
 *                                                  # see entity_info_array)
 *                hook_entity_info                  # add to module.module or inc file (if hook_entity_info already implemented,
 *                                                  # see entity_info_array)
 *
 *                + THese ONEs CONVERT TO CODE
 *                menu_entity_menu_fxn_call         # add to hook_menu
 *                views_views_api_field_array       # (embedded in views_hook_views_default_views)
 *
 *
 *    HOOK_SCHEMA
 *    /partials
 *      /{entity_machine_name}
 *        + schema_data.part              # schema definition array
 *        + schema_sub.part               # _entity_schema()
 *        + schema_sub_call.part          # $arr[] += entity_schema()
 *      /combined
 *        + schema_sub_calls.part         # hook_schema()
 *        + schema.part                   # hook_schema()
 *        
 *    HOOK_MENU
 *    + menu_data.part                # menu items array
 *    + menu_sub.part
 *    + menu_sub_call.part
 *    + menu.part                     # hook_menu()
 *
 *    HOOK_PERMISSION
 *    + permission_data.part
 *    + permission_sub.part
 *    + permission_sub_call.part
 *    + permission.part
 *
 *    HOOK_ENTITY_INFO
 *    + entity_info_data.part
 *    + entity_info_sub.part
 *    + entity_info_sub_call.part
 *    + entity_info.part
 *
 *    HOOK_VIEWS_API
 *    + views_api.part
 *
 *    HOOK_VIEWS_DATA
 *    + views_data_data.part
 *    + views_data_sub.part
 *    + views_data_sub_call.part
 *    + views_data.part
 *
 *    HOOK_VIEWS_DEFAULT_VIEWS
 *    + views_dflt_data.part
 *    + views_dflt_sub.part
 *    + views_dflt_sub_call.part
 *    + views_dflt.part
 *
 *    HOOK_UPDATE_N
 *    + update.part
 *
 *
 *
 */
    

class Entificator
{


  /**
   * Module Code Component classes
   * (Entity code component class defined in ModuleCodeComponent)
   */
  private $module_code_component_class = 'Drupal\entificator\ModuleCodeComponent';


  private $uri = '';

  /**
   * Schema importers as registered via hook_entificator_plugin()
   * @var array
   */
  private $schema_importers = array();

  /**
   * Module code component controller object
   * @var {Class extending CodeComponentBase}
   */
  private $module_code_component = null;


  /**
   * Configuration datasource scheme (eg: "entificator"); maps to 
   * the "uri_scheme" of a particular schema importer 
   * @var string
   * Eg, "entificator" from the "entificator://" in "entificator:///path/to/config.txt"
   */
  private $scheme = '';

  /**
   * Configuration datasource path (as interpreted by schema importer)
   * @var string
   * Eg, the "/path/to/config.txt" in "entificator:///path/to/config.txt"
   */
  private $path = '';

  /**
   * Schema importer object
   * @var [type]
   */
  private $importer = null;

  /**
   * Local copy of {Entificator Schema} array
   * See documentation in entificator.drush.inc
   * @var Array
   */
  private $schema = null;


  private $op_dir = null;
  private $op_path = null;

  /**
   * Array of tag names derived from "TAG_" class constants
   * @var [type]
   */
  private $tagsold = null;

  // Tags for tpl code replacements
  private $tags = array(
    'module' => array(
      //   Module
      'MODULE_MACHINE_NAME',
      'SCHEMA_SUB_CALLS',
      // 'MODULE_NAME',
      // 'MODULE_LONG_NAME',
      // 'MODULE_DESCRIPTION',
      // 'MODULE_PACKAGE',
      // 'MODULE_VERSION',
      // 'ADDITIONAL_FILE_DEPENDENCIES',
      // 'ADDITIONAL_MODULE_DEPENDENCIES',
      // 'DRUPAL_CORE_VERSION',
    ),
    'entity' => array(
      //   Entity general
      'ENTITY_MACHINE_NAME',
      
      //   Entity schema
      'SCHEMA_TABLE_NAME',

      //   Entity info
      // 'ENTITY_DOMAIN',
      // 'ENTITY_HUMAN_NAME_PLURAL',
      // 'ENTITY_KEYS',
      // 'ENTITY_LABEL',
      // 'ENTITY_LABEL_FIELD',
      // 'ENTITY_LIST_VIEW_MACHINE_NAME',
      // 'ENTITY_NAME_PLURAL',
      // 'ENTITY_NAME',
      // 'IS_FIELDABLE',

      //   Entity code outpup
      //     Schema
      'SCHEMA_TABLE_ARRAY',
      'SCHEMA_SUB_FUNC_NAME',
      'SCHEMA_SUB_CALL',
      'SCHEMA_SUB_CALLS',

      //     Other
      // 'ENTITY_CLASS',
      // 'ENTITY_CONTROLLER_CLASS',



      // hook_schema output
      // const TAG_
      // const TAG_
      // const TAG_
      // const TAG_
      // const TAG_
      // const TAG_


      // 'ENTITY_PK_FIELD',
      // 'ENTITY_TABLE_INDEXES',
      // 'ENTITY_VIEWS_DATA_ARRAY',
      // 'ENTITY_TABLE_DESCRIPTION',

      // 'FIELD_CLICK_SORT',
      // 'FIELD_FILTER_HANDLER',
      // 'FIELD_HANDLER',
      // 'FIELD_MACHINE_NAME',
      // 'FIELD_SORT_HANDLER',
      // 'FIELD_TITLE',
      // 'HOOK_VIEWS_API',
      // 'LABEL_FIELD',
      // 'LIST_VIEW_PATH',
      // 'VIEWS_TABLE_GROUP',
      // 'ENTITY_DEFAULT_URI_ROOT',

      // //  "compiled"
      // 'ENTITY_INFO_ARRAY_ITEMS',
      //         'SCHEMA_API_FIELD_ARRAYS',
      // 'SCHEMA_ARRAY',

      // 'VIEWS_DATA_ARRAY_FIELD_ARRAYS',
      // 'HOOK_VIEWS_DATA_ENTITY_FXN_CALLS',
      // 'VIEWS_DATA_CODE',

      // 'HOOK_MENU_ENTITY_MENU_FXN_CALLS',
      // 'HOOK_PERM_ENTITY_FXN_CALLS',
      // 'HOOK_SCHEMA_ARRAY_ITEMS',

      // 'MODULE_ENTITY_API_HOOKS',

      // 'ENTITY_MENU_FUNCTIONS',
      // 'ENTITY_HOOK_MENU_FUNCTION',
      // 'ENTITY_HOOK_PERM_FUNCTION',
    ),
  );

  /**
   * Flags for debug output control
   * Known keys:
   *  + all
   *    (skip items by adding -{key})
   *    eg: all,-schema
   *  + rpl
   *  + tpl-files
   *  + tpl-raw
   *  + tpl-proc
   *  + tpl-proc-all
   * @var [type]
   */
  private $debug_flags = null;

  function __construct($uri, $debug = false) {
    $this->uri = $uri;
    if($debug) $this->debug_flags = explode(',', $debug);
    print_r($this->debug_flags);
    $this->bootstrap();
  }

  private function debug($flag = null) {
    if(! $this->debug_flags) return;
    if($flag) {
      if(in_array('all', $this->debug_flags) || in_array($flag, $this->debug_flags)) {
        return ! in_array('-' . $flag, $this->debug_flags);
      }
    } else {
      if($this->debug_flags) return true;
    }
  }


  private function bootstrap() {

    // Load default "plugins"
    $plugins = array('schema_importers' => array(), 'code_component_factories' => array());
    entificator_entificator_plugin($plugins);
    // Load user plugins
    module_invoke_all('entificator_plugin', $plugins);
    //   Schema importers
    foreach($plugins['schema_importers'] as $sipkey => $sip) {
      $this->schema_importers[$sip['uri_scheme']] = $sip;
    }

  }





  private function parseUri() {
    $uri = $this->uri;
    if(!$uri) drush_set_error('Entificator', 'URI not set');
    # parse
    preg_match('/^([^:]*):\/\/(.*)/', $uri, $uri);
    $this->scheme = $uri[1];
    $this->path = $uri[2];
    if(!$this->scheme || !$this->path) drush_set_error('Entificator', 'URI not valid');
  }

  private function getImporter() {
    $importer = $this->schema_importers[$this->scheme];
    $this->importer = new $importer['class']();
    return $this->importer;
  }




  public function build() {

    // Get schema importer
    $this->parseUri();
    $importer = $this->getImporter();

    // Import schema
    $importer->import($this->path);
    $this->schema = $importer->schema;
    if($this->debug('schema')) {
      print "\n\n\n" . str_repeat('*', 60) . "\n      SCHEMA\n";
      print_r($this->schema);
      print "****  END SCHEMA  ****\n\n\n";
    }



    // Instantiate module code component object
    $cfg = array(
      'base_template_path' => $GLOBALS['conf']['entificator_root'] . '/templates/',
      'schema' => $this->schema,
      'output_path' => null,
    );

    try {
      $this->module_code_component = new $this->module_code_component_class($cfg);
    } catch (\Exception $e) {
      drush_set_error('Entificator', $e->getMessage());
      return;
    }

    // Set output path based on machine name
    $this->op_dir = $this->module_code_component->getModuleMachineName() . '/' . time();
    $this->op_path = "public://entificator/{$this->op_dir}";
    $this->module_code_component->setOutputPath($this->op_path);

    // Build
    $this->module_code_component->build();

    // Write
    $this->module_code_component->write();

    // Install/enable module
    //   Code files directory
    $files_root = drupal_realpath('public://');
    $module_code_dir = $files_root . '/entificator/' . $this->op_dir . '/module/*';
    //   Module install directory
    $module_install_directory = $this->module_code_component->getModuleInstallDir();
    //   Create dir
    drush_shell_exec('mkdir -p ' . $module_install_directory);
    //   Copy files
    $cpcmd = 'cp -r ' . $module_code_dir . ' ' . $module_install_directory;
    drush_shell_exec($cpcmd);
    //   enable
    // module_enable($module_code_component->getModuleMachineName());
    
// print_r($this->module_code_component);
die("ppppppppppppppp\n");

  }

/*
cp -r /var/www/entificator/sites/default/files/entificator/my_useful_entity_model/1399758788/module/* sites/all/modules/

  private function processTemplates() {
    $schema = &$this->schema;
    $tpls_processed = $this->templates_processed;

    $dbg = $this->debug('tpl-proc');
    $dbg_verbose = $this->debug('tpl-proc-all');



    $stub_module_files = array(
      'mod.module' => '',
      'mod.info' => '',
      'mod.api' => '',
      'mod.install' => '',
      'mod.admin.inc' => '',
      'views/mod.views.inc' => '',
      'views/mod.views_default.inc' => '',
      'lib/Entity.php' => '',
      'lib/EntityController' => '',
    );

    // Module templates/output
    foreach ($this->templates_raw['module'] as $tplkey => $tplcontents) {
      // DEBUG: Template
      if($dbg) print "\n\n\n" . str_repeat("#", 60) . "\n" . str_repeat("#", 60) . "\n\n" .
            str_repeat(' ', 10) . "PROCESSING TPL:\n" . str_repeat(' ', 10) . 
            "module.{$tplkey}\n\n\n";
      // Copy tpl contents to $templates_processed
      $this->templates_processed[$tplkey] = $tplcontents;
      $tpl = &$this->templates_processed[$tplkey];
      if($dbg) print "\n++++++++++++++++++\nTPL CONTENTS:\n  >>>>>\n" . preg_replace('/^/m', '  >>>>>   ', $tplcontents) . "  >>>>>\n++++++++++++++++++\n\n";
      // Replace tags
      $this->processTemplateReplacements($tpl);
      // Write output
      $this->writeFile($tplkey, $tplcontents);
    }

    // Entity templates/output
    foreach($this->schema['entities'] as $entkey => $entity) {
      foreach ($this->templates_raw['entity'] as $tplkey => $tplcontents) {
        // DEBUG: Template
        if($dbg) print "\n\n\n" . str_repeat("#", 60) . "\n" . str_repeat("#", 60) . "\n\n" .
              str_repeat(' ', 10) . "PROCESSING TPL:\n" . str_repeat(' ', 10) . 
              "entity[{$entkey}].{$tplkey}\n\n\n";
        // Copy tpl contents to $templates_processed
        $tpls_procd[$tplkey] = $tplcontents;
        $tpl = &$this->templates_processed[$tplkey];
        // DEBUG: Template contents
        if($dbg) print "\n++++++++++++++++++\nTPL CONTENTS:\n  >>>>>\n" . 
            preg_replace('/^/m', '  >>>>>   ', $tplcontents) . 
            "  >>>>>\n++++++++++++++++++\n\n";
        // Replace tags
        $this->processTemplateReplacements($tpl, $entkey);
        // Write output
        $this->writeFile($tplkey, $tplcontents, $entkey);
      }
    }

    die('6666');
  }

  private function processTemplateReplacements(&$tpl, $entkey = null) {
    $replacements = $this->replacements;
    $entity = $this->schema['entities'][$entkey];

    $dbg = $this->debug('tpl-proc');
    $dbg_verbose = $this->debug('tpl-proc-all');
    if($dbg_verbose) {
      print "\n++++++++++++++++++\nPRE-PROCESSED TEMPLATE:  \n";
      print "  >>>>>\n" . preg_replace('/^/m', '  >>>>>   ', $tpl) . "  >>>>>";
      print "\n++++++++++++++++++\n\n";
    }
    foreach ($this->tags['module'] as $tag) {
      if($dbg_verbose) print "TAG:  $tag -- {$replacements['module'][$tagkey]}\n";
      $tpl = str_replace('{{' . $tag . '}}', $replacements['module'][$tag], $tpl);
    }
    $op_file = $tplkey;
    $op_file = str_replace('.tpl', '', $op_file);
    $op_file = str_replace('__entity__', $entity['entity_info']['machine_name'], $op_file);
    $op_file = str_replace('__Entity__', $entity['entity_info']['camel_case'], $op_file);
    $op_file = $this->op_path . "/$op_file";
    if($dbg) print "\n++++++++++++++++++\nO/P FILE:\n$op_file\n++++++++++++++++++\n\n";
    if($dbg_verbose) {
      print "\n++++++++++++++++++\nPROCESSED TEMPLATE:  \n";
      print "  >>>>>\n" . preg_replace('/^/m', '  >>>>>   ', $tpl) . "  >>>>>";
      print "\n++++++++++++++++++\n\n";
    }

  }

  private function writeFile($tplkey, $contents, $entkey = null) {
    //file_put_contents($op_file, $tpl);
  }

  private function getReplacements() {

    $schema = &$this->schema;
  // print_r($schema);die;
    // First pass -- "basic" tags
    $replacements = &$this->replacements;

    // Module
    $replacements['module'] = array(
      'MODULE_MACHINE_NAME' => $schema['module']['machine_name'],
      // 'MODULE_NAME' => $schema['module']['human_name'],
      // 'MODULE_DESCRIPTION' => $schema['module']['description'],
      // 'MODULE_PACKAGE' => $schema['module']['package_name'],
      // 'MODULE_VERSION' => $schema['module']['version'],
      // 'ADDITIONAL_FILE_DEPENDENCIES' => $schema['module']['additional_file_dependencies'],
      // 'ADDITIONAL_MODULE_DEPENDENCIES' => $schema['module']['additional_module_dependencies'],
      // 'DRUPAL_CORE_VERSION' => $schema['module']['drupal_core_version'],

    );

    // Entities
    foreach($schema['entities'] as $entkey => $entity) {

      $replacements = &$this->replacements;
      $replacements['entities'][$entkey] = array(

        // Entity general
        'ENTITY_MACHINE_NAME' => $entkey,
        
        // Entity schema
        'SCHEMA_TABLE_NAME' => $schema['table_name'],
        // 'ENTITY_TABLE_DESCRIPTION' => $schema['table_description'],
        // 'ENTITY_PK_FIELD' => $schema['primary_key'],
        // 'ENTITY_LABEL_FIELD' => $schema['label_field'],

        // Entity info
        // 'ENTITY_HUMAN_NAME' => $entity['entity_info']['human_name'],
        // 'ENTITY_HUMAN_NAME_PLURAL' => $entity['entity_info']['human_name_plural'],
        // 'ENTITY_CLASS' => $entity['entity_info']['camel_case'],
        // 'ENTITY_CONTROLLER_CLASS' => $entity['entity_info']['camel_case'] . 'Controller',
        
        // 'LIST_VIEW_PATH' => $schema['list_view_path'],
        // 'VIEWS_TABLE_GROUP' => $schema['views_table_group'],

        // 'ENTITY_LIST_VIEW_MACHINE_NAME' => $schema['view_machine_name'],
        
        // Entity code output
        'SCHEMA_TABLE_ARRAY' => $this->buildSchemaTableArray($entity),

      );

      // Entity other
      $replacements['entities'][$entkey]['SCHEMA_SUB_FUNC_NAME'] =
            $this->buildSchemaSubName($entkey);
      $replacements['entities'][$entkey]['SCHEMA_SUB_CALL'] =
            $this->buildSchemaSubCall($entkey);


    // Module entities code
    $replacements['module']['SCHEMA_SUB_CALLS'] = $this->buildSchemaSubCalls();
  

    if($this->debug('rpl')) {
      print "\n\n\n*******************************\n    REPLACEMENTS\n*******************************\n\n";
      print_r($this->replacements);
      print "\n**************************************************************\n\n";
      
    }
  return;







      // hook_schema
      //   indexes array

      $replacements['SCHEMA_DATA_ARRAY'] = $this->buildSchemaTableArray($entity);


      // $table_idxs = rtrim($table_idxs, "\r\n");


  // $schema['lms_xora_users'] = array(
  //   'description' => 'XORA user data.',
  //   'primary key' => array('id'),
  //   'indexes' => array(
  //     'userId' => array('userId'),
  //   ),
  //   'fields' => array(
  //     '
      //   field arrays
      $hook_schema_fld_arrays = $this->buildSchemaFieldsArray($entity);

    }

    // hook_views_api - populate from template
    $replacements['HOOK_VIEWS_API'] = $this->template_files['partials/views_hook_views_api'];



    $replacements['ENTITY_VIEWS_DATA_ARRAY'] = $this->buildViewsDataArray();

    $replacements['ENTITY_HOOK_MENU_FUNCTION'] = $schema['XXXXXX'];

    $replacements['ENTITY_HOOK_PERM_FUNCTION'] = $schema['XXXXXX'];

    $readme_add_to_module = $this->moduleReadMeAddToModule();
  // print_r($replacements);die('asdf');

  }

*/



}
