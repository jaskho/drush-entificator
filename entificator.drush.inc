<?php
/**
 * Entificator generates entity module code from imported configuration.
 * Running the command will generate both:
 *   + A ready-to-deploy standalone module implementing your entity.
 *   + A collection of code fragments (and instructions) for adding your entity
 *     to an existing module.
 * Supported configuration sources include
 *   + Text file
 *   + (@TODO) Existing SQL table
 *   + (@TODO) SQL CREATE TABLE statement
 *   + You name it: Entificator provides a plugin framework for adding custom
 *     schema importers.
 *
 * Comparison to similar tools.
 *   + ECK. Using Entificator is similar to exporting an ECK entity to a 
 *     Features module. 
 *     Differences:
 *       + No dependency on ECK or Features.
 *       + Arbitrary table structure--no bundles, no relations with eck_entity_type
 *         and eck_bundle. This can be nice in cases when you already have tabular
 *         data and you'd just like to use it as-is.
 *       + Easily (well, relatively speaking) add new a new entity to an existing
 *         application without having to add yet another module.
 *     Disclaimer: ECK is in many important ways a better solution for creating
 *       custom entities. If you're not sure which method you should use, or you
 *       are more of a "site builder" than a "site developer", go with ECK.
 *       On the other hand, if you're a developer looking for a way to
 *         + Rapidly build or prototype an entity application
 *         + Add an entity to an existing module
 *       while **avoiding the need to integrate with ECK and Features**,
 *       Entificator might come in handy.
 *   + The "Model" example entity module.  One loose way to think about 
 *     Entificator is as a largely automated version of what the Model project
 *     does. Model provides a template entity module that developers can
 *     use as a basis for their own module. Entificator provides a similar
 *     "template" but does the copy/paste/modify/extend-ing for you.
 *
 * Background.
 * Implementing a CRUD- and Views-enabled entity requires metadata configuration 
 * in mutliple locations:
 *   hook_entity_info
 *   hook_entity_info_alter
 *   hook_entity_property_info
 *   hook_entity_property_info_alter
 *   hook_schema
 *   hook_menu
 *   hook_permission
 *   hook_views_data
 *   hook_views_default_views
 * This makes for a development process that's nighmarish for the un-
 * initiated, and tedious and error-prone even for old pros.
 * The Entity Construction Kit provides a slick and full-featured, UI-based tool
 * for creating custom entities without all that fuss, but adds layers of
 * abstraction that can be overkill, and can turn what should be a straightforward
 * application into a confusing (or just tedious) beast.
 *   
 * Entificator aims to DRY-ify (and otherwise slick up) the process of creating
 * an entity by
 *   + Placing all configuration in one place, and eliminating redundancy.
 *   + Minimizing the number of required configuration elements. 
 *     All that's required to create an entity module or module extension
 *     with full CRUD and Views support is a table name and some field
 *     names. Of course, the defaults supplied may not meet your needs,
 *     and the resulting UI will be utilitarian, but you will at least be
 *     in business.
 *     (And of course you're not limited to the defaults. Most of the the 
 *     parameters you'd typically want to control are fully configurable).
 *
 *  
 * Entificator does not attempt to support every use-case for working with 
 * Drupal entities.  If your application is complex you might be better off
 * using ECK, or building from scratch. But Entificator may be helpful even in 
 * complex situations, to give you a nice running start on development on a 
 * new module, or an addition to an existing one.
 *
 */

/**
 * Entificator data structures and logic
 *
 * Entificator's central use-case is to expose tabular data as a Drupal entity.
 * Thus, Entificator "thinks" in terms of tables and columns as the primary
 * structural elements, building the entity/properties around it.
 *
 * Configuration is held in a structured array, the {Entificator Schema} array.
 *
 * {Entificator Schema} array
 * Keys:
 *   + module
 *       {Entificator Module Definition} array
 *   + entities
 *      Array of {Entificator Entity Definition} arrays, keyed by entity
 *      machine_name
 *
 * {Entificator Module Definition} array
 *   Keys:
 *     + machine_name
 *     + install_enable
 *     + sites_dir
 *     + sub_dir
 *     + human_name
 *     + description
 *     + drupal_core_version
 *     + package_name
 *     + version
 *     + module_dependencies
 *     + file_dependencies
 *     
 * {Entificator Entity Definition} array
 *   Keys:
 *     + schema (hook_schema)
 *       {Entificator Db Schema} array
 *     + entity_info (hook_entity_info(), hook_entity_info_alter())
 *       {Entificator Entity Info} array
 *     + property_info
 *       {Entificator Property Info} array
 *       Array of entity property definitions, keyed by property_key (extending column definitions)
 *     + bundles
 *       {Entificator Bundles Info} array
 *     + fields
 *       {Entificator Fields Info} array
 *
 * {Entificator Db Schema} array
 *   Keys:
 *     + indexes
 *     + columns
 *       Array of column definitions, keyed by column name, with Keys
 *       mapping to hook_schema['fields'] items.
 *
 * {Entificator Entity Info} array
 * Corresponds, roughly, to data format in hook_entity_info()
 *   Keys:
 *
 * {Entificator Property Info} array
 * Corresponds, roughly, to data format in hook_entity_property_info()
 * and hook_entity_property_info_alter()
 *   Keys:
 *     + property_info (hook_entity_property_info, hook_entity_property_info_alter())
 *       Array of entity property definitions (extending column definitions)
 *       Keys map to 
 *
 * {Entificator Bundles} array
 *     + bundles
 *
 * {Entificator Fields} array
 *     + fields
 *
 */
  

/**
 * Schema importers
 * Entificator implements a pluggable architecture for defining schema/config
 * importers.
 * See
 *  + entificator_entificator_plugin()
 *  + EntificatorSchemaImporter base class
 *  + EntificatorDefaultImporter default importer class
 *
 * A schema importer implements a class extending EntificaterSchemaImporter.
 * This class takes an implementation-specific configuration data source 
 * (in the default importer this is a path to an EntificatorDefaultImporter
 * configuration file).  It queries this source and uses it to build a structured
 * "Entificator Schema" array (see above), which Entificator then usese to 
 * generate module code.
 *
 * Schema importers implement a uri "scheme". Eg., the default importer defines
 * "entificator".
 *
 * Entificator loads a particular importer based on its calling "schemasource" parameter,
 * e.g.:
 *    drush entificator entificator:///path/to/config.txt
 *
 */


/*
 Using Entificator to add an entity to an existing module.
 
    Adding an Entificator-generated entity to an existing module typically involves
    a number of straightforward copy/paste operations.

    For developers who know their way around hook_menu, hook_install, and the 
    Entity and Views APIs, the "QUICK STEPS" instructions plus common sense 
    (eg., you may need to rename a variable here or there) should 
    hopefully suffice.

    For those less familiar in these areas, or if you run into trouble, consult the 
    detailed instructions, below.  



    ######    QUICK STEPS    ######

    + Add these directories to your module root directory:
        path/to/MODULE_NAME/
         + includes
         + lib
         + views

    + Add Entificator-generated files to your module (as necessary/desired):

        MODULE_NAME.install
        lib/
          EntityName.php
          EntityNameController.php
        includes/
          my_entity.entity.php
        views 
          MODULE_NAME.views.inc
          MODULE_NAME.views_default.inc

    + Add integrating code 

      -- Core/Entity/Views Hooks --

      In general, you have three options for implementing each hook involved
      in adding an entity to an existing module:
        1.  Whole file.  Use an Entificator-generated file containing the hook 
            implementation (adding it to your module) (where possible).
        2.  Just the hook.  Use an Entificator-generated hook implementation 
            (adding it to your existing file).
        3.  Just the code.  Use Entificator-generated code (adding it to your 
            existing hook implementation).

      HOOK           Add this     Or add this hook  Or add this code to
                     file to      function to       your hook function
                     your module  your file
      -----------    -----------  ---------------   ------------------------

      schema         M.install    schema.part       schema_sub_call.part 

      menu           N/A          menu.part         menu_sub_call.part 

      permission     N/A          permission.part   permission_sub_call.part 

      entity_info    N/A          ent_info.part     ent_info_sub_call.part 

      views_api      N/A          views_api.part    (No action required if hook
                                                     already in place)

      views_data     M.views.inc  views_data.part   views_data_sub_call.part   

      views_         M.views_     views_dflt.part   views_dflt_sub_call.part 
      default_views  default.inc           

      update_N       M.install    update.part       N/A 
      
      
      -- Schema definition function --

      To support Drupal\'s module uninstallation process, Entificator\'s 
      entity schema definition function should be placed into the 
      MODULE_NAME.install file.

      If you\'re using Entificator\'s .install file, you\'re good to go. Otherwise,
      copy the contents of schema_sub_fxn.part into your existing .install file.


      -- MODULE.info --

      Add any new files to your .info file.
      Copy the relevant lines from info.part.


    + Visit /update.php or otherwise run update.
      








    ######    DETAILED INSTRUCTIONS    ######


    *******************************************
    ***   NOTE: module hooks                ***
    *******************************************
    Entificator factors entity-specific code for handling upstream hooks into separate functions,
    collected in an entity-specific include file ({module_root}/includes/my_entity.entity.inc).
    This pattern is used for:
      + hook_menu
      + hook_permission
      + hook_schema (*)
      + hook_entity_info
      + hook_views_data
      + hook_views_default_views
    (*) In the case of hook_schema, Entificator still factors the entity\'s 
        hook-related code into a separate function, but places this function into the 
        MODULE_NAME.install file in order to avoid potential problems with inc 
        file loading at module uninstall time.

    For instance, in hook_menu, we have:
        <code>
          // (All your existing menu item arrays)
          $items[] = ...
          ...
          // Get my_entity menu items
          $items += _my_module_my_entity_entity_menu_items();
        </code>
      Then, in includes/my_entity.entity.inc we have:
        <code>
          function _my_module_my_entity_entity_menu_items() {
            // Define CRUD paths for this entity
            $items[...] = ...
            ...
            return $items;
          }
        </code>
    This results in code that the author finds esthetically pleasing, but you are of course free
    to do it another way, and should be able to do so with a straightforward copy/paste.


    *******************************************
    ***   STEP 1:  Add supporting files     ***
    *******************************************

    + Add new directories to your module root directory (if necessary):
        path/to/MODULE_NAME/
         + includes
         + lib
         + views

    + Schema API support (if necessary)
       (If your module already implements a .install file, skip this step)
       + Copy:
          + MODULE_NAME.install
         To:
            path/to/MODULE_NAME/
             + MODULE_NAME.install

    + Entity functions
       + Copy:
          + my_entity.entity.php 
         To:
            path/to/MODULE_NAME/includes/
             + my_entity.entity.php
       + Add this line to your MODULE_NAME.info file:
          + files[] = includes/my_entity.entity.php

    + Entity class files
       + Copy:
          + MyEntity.php 
          + MyEntityController.php
         To:
            path/to/MODULE_NAME/lib/
             + MyEntity.php
             + MyEntityController.php
       + Add these lines to your MODULE_NAME.info file:
          + files[] = lib/MyEntity.php
          + files[] = lib/MyEntityController.php

    + Views support (if necessary)
       (If your module already implements hook_views_data or hook_views_default_views, skip the
       relevant step)
       + Copy:
          + MODULE_NAME.views.inc
          + MODULE_NAME.views_default.inc
         To:
            path/to/MODULE_NAME/views/
             + MODULE_NAME.views.inc
             + MODULE_NAME.views_default.inc



    *******************************************
    ***   STEP 2:  Wire it up     ***
    *******************************************

    + Schema API integration
       If you added a NEW MODULE_NAME.install file above
         No action is required.

       OTHERWISE:
         + -- Add hook_update function --
           Add the contents of 
            + hook_update_7100_function.part
           To the body of the existing
            + MODULE_NAME.install file.
           If necessary, modify the "7100" part of the function name as appropriate.
           (If your module has no previous hook_update functions in place, you can 
            safely leave the function name as-is.  For more information, see 
            https://api.drupal.org/api/drupal/modules%21system%21system.api.php/function/hook_update_N/7)

         + -- Add hook_schema function or code --
           If your module previously had a .install file, but hook_schema was not implemented:
             + Add hook_schema function
               Add the contents of 
                + hook_schema_function.part 
               To the body of the existing 
                + MODULE_NAME.install file.

           OTHERWISE: (ie., hook_schema IS already defined)
             + Add call to schema function to hook_schema
               Add the contents of 
                + schema_items_function_call.part
               To the body of the existing
                + MODULE_NAME_schema(){} function.
               If necessary, modify the new and/or existing logic as appropriate.


    + Entity API integration


    + Menu API integration
       If your module already implements hook_menu():
         Add the contents of 
          + menu_item_function_call.part 
         To the body of the existing
          + MODULE_NAME_menu(){} function.
         If necessary, modify the new and/or existing logic as appropriate.

       OTHERWISE: (ie., hook_menu is NOT already defined)
         Add the contents of 
          + hook_menu_function.part 
         To the body of the existing
          + MODULE_NAME.module file


    + Access API integration
       If your module already implements hook_permission():
         Add the contents of 
          + permission_function_call.part 
         To the body of the existing
          + MODULE_NAME_permission(){} function.
         If necessary, modify the new and/or existing logic as appropriate.

       OTHERWISE: (ie., hook_menu is NOT already defined)
         Add the contents of 
          + hook_menu_function.part 
         To the body of the existing
          + MODULE_NAME.module file


    + Views API integration
       + -- Enable Views API --
         If your module already implements hook_views_api():
           No action is required.

         OTHERWISE: (ie., hook_views_api is NOT already defined)
           Add the contents of 
            + hook_views_api_function.part 
           To the body of the existing
            + MODULE_NAME.module file

       + -- HOOK_VIEWS_DATA --
         If you added a NEW MODULE_NAME.views.inc file in Step 1
          No action is required.

         OTHERWISE: (ie., your module already has a MODULE_NAME.views.inc file)
           If your module already implements hook_views_data():
             Add the contents of 
              + views_data_function_call.part 
             To the body of the existing
              + MODULE_NAME_views_data(){} function.
             If necessary, modify the new and/or existing logic as appropriate.

           OTHERWISE: (ie., hook_views_data() is NOT already defined)
             Add the contents of 
              + views_data_function.part 
             To the body of the existing
              + MODULE_NAME.views.inc file

       + -- HOOK_VIEWS_DEFAULT_VIEWS --
         If you added a NEW MODULE_NAME.views_default.inc file in Step 1
          No action is required.

         OTHERWISE: (ie., your module already has a MODULE_NAME.views_default.inc file)
           If your module already implements hook_views_default_views():
             Add the contents of 
              + views_default_view_function_call.part 
             To the body of the existing
              + MODULE_NAME_views_default_views(){} function.
             If necessary, modify the new and/or existing logic as appropriate.

           OTHERWISE: (ie., hook_views_default_views() is NOT already defined)
             Add the contents of 
              + views_default_view_function.part 
             To the body of the existing
              + MODULE_NAME.views_default.inc file


    + Module.info files[] items
      As appropriate, add the following lines to your MODULE_NAME.info file
        + files[] = lib/MyEntity.php
        + files[] = lib/MyEntityController.php
        + files[] = includes/my_entity.entity.php

    ';
 */

/**
 * Configuration -> Processing -> Code generation workflow
 *
 *    { Entificator Controller }
 *                   |  |         
 *                   |  |                
 *                   |  |            --> { Configuration Source }      
 *                   |  |            |                                    
 *                   |  |            |                                    
 *                   |  |                                   
 *                   |  |----> { SchemaImporter }             
 *                   |               |
 *           |       |               |
 *                   |               --> { Entificator schema array } <----
 *                   |                                                    |
 *                   |                                                    |
 *                   |                                                    |
 *                   ------------------> { CodeComponent Configuration Array }                        
 *           |                  ------->
 *           |                  |
 *           |       ______________________________________                               
 *           |----> {  { ModuleCodeComponent }             }
 *                  {   |                                  }
 *                  {   |                                  }
 *                  {   |----> { EntityCodeComponent(s) }  }
 *                   ______________________________________                               
 *
 *
 *    Data in form of              Is used to generate             By
 *    -----------------            --------------------            ------------------
 *    
 *    Configuration source ------> Entificator schema              SchemaImporter
 *                                      |                           + Entificator Controller
 *                                      |
 *        -------------------------------
 *        |
 *       \|/
 *    Entificator schema --------> CodeComponent Configuration     Entificator Controller
 *                                      |
 *        -------------------------------
 *        |
 *       \|/
 *    CodeComponent Config'n ----> Parsed CodeComp't Config'n      ModuleCodeComponent 
 *                                      |                           + EntityCodeComponent(s)
 *                                      |
 *        -------------------------------
 *        |
 *       \|/
 *    Parsed CodeComp't Cfg'n ---> Token replacements              ModuleCodeComponent 
 *                                      |                           + EntityCodeComponent(s)
 *                                      |
 *        -------------------------------
 *        |
 *       \|/
 *    Token replacements --------> Code files                      ModuleCodeComponent 
 *      PLUS                            |                           + EntityCodeComponent(s)
 *    Template files                    | 
 *                                      |
 *        -------------------------------
 *        |
 *       \|/
 *    Code files ----------------> Deployed module                 Entificator Controller 
 *                                   AND/OR
 *                                 Code partials for integration
 *                                 w/ existing module
 *                                      |
 *        -------------------------------
 *        |
 *       \|/
 *    Deployed module -----------> Glory, wealth and fame           You & yours 
 * 
 * 
 * We start with a
 *   <<Entificator Controller>> (\entificator\Entificator) object
 *   <<Configuration Source>>
 *
 * The <<Entificator Controller>> instantiates a
 *   <<SchemaImporter>>
 *
 * The <<SchemaImporter>> loads/queries the <<Configuration Source>>
 * and creates a
 *   <<Entificator schema array>> data structure
 *
 * The <<Entificator Controller>> then parses the <<Entificator schema array>>
 * into a
 *   <<CodeComponent Configuration Array>>
 * 
 * And uses the <<CodeComponent Configuration Array>> to instantiate a
 *   <<ModuleCodeComponent>>
 *
 * The <<ModuleCodeComponent>> then...
 *   + Instantiates child <<EntityCodeComponent>> objects
 *   + Applies default/dynamic values to the <<CodeComponent Configuration Array>>
 *   + Loads template files
 *   + Populates template token replacement values
 *   + Generate output code files by applying the token replacements to the templates
 *   + (Optionally) deploys and enables module
 * 
 */

// Load supporting files
require_once 'lib/builder/CodeComponentBase.php';
require_once 'lib/entificator/ModuleCodeComponent.php';
require_once 'lib/entificator/EntityCodeComponent.php';
require_once 'lib/entificator/Entificator.php';
require_once 'lib/entificator/EntificatorSchemaImporter.php';
require_once 'lib/entificator/EntificatorDefaultImporter.php';
require_once 'plugins/entificator.php';

use Drupal\Entificator\Entificator;
use Drupal\builder as builder;

function entificator_drush_command() {

  $items = array();
  $items['entificate'] = array(
    'description' => "Generates code for adding an entity to a module.",
    'arguments' => array(
      'schemasource' => 'URI for schema data source, eg. entificator://sites/all/modules/mymodule/schema.dat',
      'debug' => 'Comma-delimited debug flags (correspond to Entificator class properties; see code for specifics.',
    ),
    'required-arguments' => 1,
    // 'options' => array(
    //   'spreads' => 'Comma delimited list of spreads (e.g. mayonnaise, mustard)',
    // ),
    'examples' => array(
      'drush ent entificator:///path/to/schema/schema.txt' => 'Generate module code using the default schema importer (hence "entificator://") to parse the configuration contained in the file "/path/to/schema/schema.txt")',
    ),
    'aliases' => array('ent'),
    'bootstrap' => DRUSH_BOOTSTRAP_FULL, // No bootstrap at all.
    'drupal dependencies' => array('entity'),
  );

  return $items;
}

function drush_entificator_entificate($uri, $debug = false) {
  $GLOBALS['conf']['entificator_root'] = __DIR__;
  $ent = new Entificator($uri, $debug);
  $ent->build();
}
