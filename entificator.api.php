<?php

/**
 * implements hook_entificator_plugin
 * 
 * Return array of Entificator plugins, organized by plugin type.
 * EG:
 *  $plugins = array(
 *    'schema_importer' => array(
 *      'importer_1' => array(
 *        'uri_scheme' => 'entificator',
 *      ),
 *    ),
 *  );
 */
function hook_entificator_plugin() {
  $plugins = array(
    # Plugin type key (currently only "schema_importer" is supported)
    'schema_importer' => array(
      # Key for this importer. Must be unique among plugins in its type;
      # in order to avoid name collisions prefix with module name
      'entificator_default_importer_1' => array(
        'uri_scheme' => 'entificator',
        'class' => 'EntificatorDefaultImporter', 
      ),
    ),
  );
}