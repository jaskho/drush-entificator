<?php

/**
 * implements hook_entificator_plugin
 * 
 * Custom schema format
 * syntax:
 *   entity_name:Entity Name
 *   fld1_name:Fld 1 Label:Fld1 Type:Fld1 Len
 *   fld2_name:Fld 2 Label:Fld2 Type:Fld2 Len
 */
function entificator_entificator_plugin(&$plugins = array()){
  $plugins['schema_importers']['default_schema_importer'] = array(
    'uri_scheme' => 'entificator',
    'class' => 'Drupal\entificator\EntificatorDefaultImporter',
  );
}
