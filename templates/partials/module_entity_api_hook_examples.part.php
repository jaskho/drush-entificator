<?php
/**
 * @addtogroup hooks
 * @{
 */
/**
 * Acts on {{ENTITY_NAME_PLURAL}} being loaded from the database.
 *
 * This hook is invoked during {{ENTITY_HUMAN_NAME}} loading, which is handled by
 * entity_load(), via the EntityCRUDController.
 *
 * @param array {{ENTITY_MACHINE_NAME}}s
 *   An array of {{ENTITY_HUMAN_NAME}} entities being loaded, keyed by id.
 *
 * @see hook_entity_load()
 */
function hook_{{ENTITY_MACHINE_NAME}}_load(array ${{ENTITY_MACHINE_NAME}}s) {
  $result = db_query('SELECT pid, foo FROM {mytable} WHERE pid IN(:ids)', array(':ids' => array_keys(${{ENTITY_MACHINE_NAME}}s)));
  foreach ($result as $record) {
    ${{ENTITY_MACHINE_NAME}}s[$record->pid]->foo = $record->foo;
  }
}
/**
 * Responds when a {{ENTITY_HUMAN_NAME}} is inserted.
 *
 * This hook is invoked after the {{ENTITY_HUMAN_NAME}} is inserted into the database.
 *
 * @param {{ENTITY_CLASS}} {{ENTITY_MACHINE_NAME}}
 *   The {{ENTITY_HUMAN_NAME}} that is being inserted.
 *
 * @see hook_entity_insert()
 */
function hook_{{ENTITY_MACHINE_NAME}}_insert({{ENTITY_CLASS}} {{ENTITY_MACHINE_NAME}}) {
  db_insert('mytable')
    ->fields(array(
      'id' => entity_id('{{ENTITY_MACHINE_NAME}}', {{ENTITY_MACHINE_NAME}}),
      'extra' => print_r({{ENTITY_MACHINE_NAME}}, TRUE),
    ))
    ->execute();
}
/**
 * Acts on a {{ENTITY_HUMAN_NAME}} being inserted or updated.
 *
 * This hook is invoked before the {{ENTITY_HUMAN_NAME}} is saved to the database.
 *
 * @param {{ENTITY_CLASS}} {{ENTITY_MACHINE_NAME}}
 *   The {{ENTITY_HUMAN_NAME}} that is being inserted or updated.
 *
 * @see hook_entity_presave()
 */
function hook_{{ENTITY_MACHINE_NAME}}_presave({{ENTITY_CLASS}} {{ENTITY_MACHINE_NAME}}) {
  {{ENTITY_MACHINE_NAME}}->name = 'foo';
}
/**
 * Responds to a {{ENTITY_HUMAN_NAME}} being updated.
 *
 * This hook is invoked after the {{ENTITY_HUMAN_NAME}} has been updated in the database.
 *
 * @param {{ENTITY_CLASS}} {{ENTITY_MACHINE_NAME}}
 *   The {{ENTITY_HUMAN_NAME}} that is being updated.
 *
 * @see hook_entity_update()
 */
function hook_{{ENTITY_MACHINE_NAME}}_update({{ENTITY_CLASS}} {{ENTITY_MACHINE_NAME}}) {
  db_update('mytable')
    ->fields(array('extra' => print_r({{ENTITY_MACHINE_NAME}}, TRUE)))
    ->condition('id', entity_id('{{ENTITY_MACHINE_NAME}}', {{ENTITY_MACHINE_NAME}}))
    ->execute();
}
/**
 * Responds to {{ENTITY_HUMAN_NAME}} deletion.
 *
 * This hook is invoked after the {{ENTITY_HUMAN_NAME}} has been removed from the database.
 *
 * @param {{ENTITY_CLASS}} {{ENTITY_MACHINE_NAME}}
 *   The {{ENTITY_HUMAN_NAME}} that is being deleted.
 *
 * @see hook_entity_delete()
 */
function hook_{{ENTITY_MACHINE_NAME}}_delete({{ENTITY_CLASS}} {{ENTITY_MACHINE_NAME}}) {
  db_delete('mytable')
    ->condition('pid', entity_id('{{ENTITY_MACHINE_NAME}}', {{ENTITY_MACHINE_NAME}}))
    ->execute();
}

/**
 * Act on a {{ENTITY_HUMAN_NAME}} that is being assembled before rendering.
 *
 * @param ${{ENTITY_MACHINE_NAME}}
 *   The {{ENTITY_HUMAN_NAME}} entity.
 * @param $view_mode
 *   The view mode the {{ENTITY_HUMAN_NAME}} is rendered in.
 * @param $langcode
 *   The language code used for rendering.
 *
 * The module may add elements to ${{ENTITY_HUMAN_NAME}}->content prior to rendering. The
 * structure of ${{ENTITY_HUMAN_NAME}}->content is a renderable array as expected by
 * drupal_render().
 *
 * @see hook_entity_prepare_view()
 * @see hook_entity_view()
 */
function hook_{{ENTITY_MACHINE_NAME}}_view(${{ENTITY_MACHINE_NAME}}, $view_mode, $langcode) {
  ${{ENTITY_MACHINE_NAME}}->content['my_additional_field'] = array(
    '#markup' => $additional_field,
    '#weight' => 10,
    '#theme' => 'mymodule_my_additional_field',
  );
}
/**
 * Alter the results of entity_view() for {{ENTITY_NAME_PLURAL}}.
 *
 * @param $build
 *   A renderable array representing the {{ENTITY_HUMAN_NAME}} content.
 *
 * This hook is called after the content has been assembled in a structured
 * array and may be used for doing processing which requires that the complete
 * {{ENTITY_HUMAN_NAME}} content structure has been built.
 *
 * If the module wishes to act on the rendered HTML of the {{ENTITY_HUMAN_NAME}} rather than
 * the structured content array, it may use this hook to add a #post_render
 * callback. Alternatively, it could also implement hook_preprocess_{{ENTITY_HUMAN_NAME}}().
 * See drupal_render() and theme() documentation respectively for details.
 *
 * @see hook_entity_view_alter()
 */
function hook_{{ENTITY_MACHINE_NAME}}_view_alter($build) {
  if ($build['#view_mode'] == 'full' && isset($build['an_additional_field'])) {
    // Change its weight.
    $build['an_additional_field']['#weight'] = -10;
    // Add a #post_render callback to act on the rendered HTML of the entity.
    $build['#post_render'][] = 'my_module_post_render';
  }
}

/**
 * Alter {{ENTITY_HUMAN_NAME}} forms.
 *
 * Modules may alter the {{ENTITY_HUMAN_NAME}} entity form by making use of this hook.
 * #entity_builders may be used in order to copy the values of added form
 * elements to the entity, just as documented for
 * entity_form_submit_build_entity().
 *
 * @param $form
 *   Nested array of form elements that comprise the form.
 * @param $form_state
 *   A keyed array containing the current state of the form.
 */
function hook_form_{{ENTITY_MACHINE_NAME}}_form_alter(&$form, &$form_state) {
  // Your alterations.
}

/**
 * @}
 */
