<?php

/**
* {{ENTITY_CLASS}} entity class.
*/
class {{ENTITY_CLASS}} extends Entity {
  protected function defaultLabel() {
    return $this->${{ENTITY_LABEL_FIELD}};
  }
  protected function defaultUri() {
    return array('path' => '{{ENTITY_DEFAULT_URI_ROOT}}/' . $this->identifier());
  }
}
