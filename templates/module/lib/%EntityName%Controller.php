<?php
/**
 * {{ENTITY_CLASS}} entity controller
 */
class {{ENTITY_CLASS}}Controller extends EntityAPIController {
  public function create(array $values = array()) {

    // Init entity property fields
    $properties_info = entity_get_property_info('{{ENTITY_MACHINE_NAME}}');
    $default_values = array_fill_keys(array_keys($properties_info['properties']), NULL);

    // Merge defaults
    $values += $default_values;
 
    return parent::create($values);
  }

  /**
   * Implements EntityAPIControllerInterface::buildContent
   * Build render array for entity
   */
  public function buildContent($entity, $view_mode = 'full', $langcode = NULL, $content = array()) {

    $wrapper = entity_metadata_wrapper('{{ENTITY_MACHINE_NAME}}', $entity);

    // Loop entity property fields to create default render items
    $properties_info = entity_get_property_info('ENTITY_MACHINE_NAME');
    $properties_info = $properties_info['properties'];
    // Ignore "url" field added by entity system
    unset($properties_info['url']);

    foreach($properties_info as $pkey => $pinfo) {

      $content[$pkey] = array(
        '#theme' => 'field',
        '#weight' => 0,
        '#title' =>t($pinfo['label']),
        '#access' => TRUE,
        '#label_display' => 'inline',
        '#view_mode' => 'full',
        '#language' => LANGUAGE_NONE,
        '#field_name' => 'field_fake_description',
        '#field_type' => 'text',
        '#entity_type' => '{{ENTITY_MACHINE_NAME}}',
        '#bundle'  => '',
        '#items' => array(
          array(
            'value' => $entity->$pkey,
          )
        ),
        '#formatter' => 'text_default',
        0 => array('#markup' => check_plain($entity->$pkey)),
      );      
    }

    return parent::buildContent($entity, $view_mode, $langcode, $content);
  }
}
